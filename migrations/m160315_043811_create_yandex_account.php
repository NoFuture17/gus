<?php

class m160315_043811_create_yandex_account extends \app\migrations\Migration
{
    public $tableName = '{{%yandex_account}}';

    public function up()
    {
        $columns = [
            'id'                => $this->primaryKey(),
            'active'            => $this->smallInteger(),
            'name'              => $this->string(255),
            'password'          => $this->string(255),
            'translate_token'   => $this->string(255),
        ];

        $generator = new \app\migrations\generators\Generator($this, $this->tableName);
        $generator->additionalColumns = $columns;
        $generator->create();
    }

    public function down()
    {
        $this->dropTable($this->tableName);
    }
}
