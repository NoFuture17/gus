<?php
/**
 * Created by PhpStorm.
 * User: nofuture17
 * Date: 04.12.15
 * Time: 19:51
 */

namespace app\migrations\tpl;

use \app\migrations\generators;

class create_redirect extends \app\migrations\Migration
{
    public $redirectTableName = '{{%redirect}}';

    public function up()
    {
        $userGenerator = new generators\Redirect($this, $this->redirectTableName);
        $userGenerator->create();
    }

    public function down()
    {
        $this->dropTable($this->redirectTableName);
    }
}