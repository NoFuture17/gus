<?php

namespace app\migrations\tpl;

use yii\db\Schema;
use yii\db\Migration;
use \app\migrations\generators\Page as PageGenerator;

class create_page extends \app\migrations\Migration
{
    public $pageTableName = '{{%page}}';
    
    public function up()
    {
        // Создание таблицы страниц
        $pageGenerator = new PageGenerator($this, $this->pageTableName);
        $pageGenerator->create();
    }

    public function down()
    {
        $this->dropTable($this->pageTableName);
    }
}
