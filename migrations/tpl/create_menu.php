<?php
/**
 * Created by PhpStorm.
 * User: nofuture17
 * Date: 07.12.15
 * Time: 15:23
 */

namespace app\migrations\tpl;

use \app\migrations\generators;

class create_menu extends \app\migrations\Migration
{
    public $menuTableName = '{{%menu}}';
    public $menuItemTableName = '{{%menu_item}}';

    public function safeUp()
    {
        // Создаём таблицу меню
        $menuGenerator = new generators\Menu($this, $this->menuTableName);
        $menuGenerator->create();

        // Создаём таблицу элементов меню
        $menuItemGenerator = new generators\MenuItem($this, $this->menuItemTableName);
        $menuItemGenerator->create();

        // Главное меню
        $this->insert(
            $this->menuTableName,
            [
                'name'      => 'Главное меню',
                'section'   => 'main',
                'active'    => 1,
            ]
        );
    }

    public function safeDown()
    {
        $this->dropTable($this->menuItemTableName);
        $this->dropTable($this->menuTableName);
    }
}