<?php
namespace app\migrations\tpl;

use yii\db\Schema;
use \app\migrations\generators;
use \app\components\widgets\form\SmartInput;

class create_param extends \app\migrations\Migration
{
    public $tableName = '{{%param}}';
    public $languageTableName = '{{%language}}';

    public function up()
    {
        $paramGenerator = new generators\Param($this, $this->tableName);
        $paramGenerator->create();

        $languageGenerator = new generators\Language($this, $this->languageTableName);
        $languageGenerator->create();

        $this->insert(
            $this->tableName,
            [
                'param'     => 'THEME',
                'active' => \app\components\models\SmartRecord::ACTIVE_ACTIVE,
                'value'     => 'base',
                'default'   => 'base',
                'name'      => 'Тема сайта',
                'type'      => 'theme',
                'input_type'=> SmartInput::TYPE_SELECT,
            ]
        );

        $this->insert(
            $this->tableName,
            [
                'param'     => 'APP.NAME',
                'active' => \app\components\models\SmartRecord::ACTIVE_ACTIVE,
                'value'     => 'Новый сайт',
                'default'   => 'Новый сайт',
                'name'      => 'Имя сайта',
                'type'      => 'string',
                'input_type'=> SmartInput::TYPE_TEXT,
            ]
        );

        $this->insert(
            $this->tableName,
            [
                'param'     => 'PARAM.ADMIN_EMAIL',
                'active' => \app\components\models\SmartRecord::ACTIVE_ACTIVE,
                'value'     => 'webmaster-amd@yandex.ru',
                'default'   => 'webmaster-amd@yandex.ru',
                'name'      => 'Email администратора',
                'type'      => 'string',
                'input_type'=> SmartInput::TYPE_TEXT,
            ]
        );

        $this->insert(
            $this->tableName,
            [
                'param'     => 'PARAM.SUPPORT_EMAIL',
                'active' => \app\components\models\SmartRecord::ACTIVE_ACTIVE,
                'value'     => 'webmaster-amd@yandex.ru',
                'default'   => 'webmaster-amd@yandex.ru',
                'name'      => 'Email поддержки',
                'type'      => 'string',
                'input_type'=> SmartInput::TYPE_TEXT,
            ]
        );

        $this->insert(
            $this->tableName,
            [
                'param'     => 'PARAM.HOST',
                'active' => \app\components\models\SmartRecord::ACTIVE_ACTIVE,
                'value'     => 'example.com',
                'default'   => 'example.com',
                'name'      => 'Хост',
                'type'      => 'string',
                'input_type'=> SmartInput::TYPE_TEXT,
            ]
        );

        $this->insert(
            $this->tableName,
            [
                'param'     => 'PARAM.DATA_TEMP_DIR',
                'active' => \app\components\models\SmartRecord::ACTIVE_ACTIVE,
                'value'     => '/data/temp/',
                'default'   => '/data/temp/',
                'name'      => 'Путь к временной папке (относительно web)',
                'type'      => 'string',
                'input_type'=> SmartInput::TYPE_TEXT,
            ]
        );

        $this->insert(
            $this->tableName,
            [
                'param'     => 'MAILER.FILE_TRANSPORT',
                'active' => \app\components\models\SmartRecord::ACTIVE_ACTIVE,
                'value'     => 1,
                'default'   => 1,
                'name'      => 'Писать почту в файл',
                'type'      => 'boolean',
                'input_type'=> SmartInput::TYPE_CHECKBOX,
            ]
        );

        $this->insert(
            $this->tableName,
            [
                'param'     => 'MAILER.HOST',
                'active' => \app\components\models\SmartRecord::ACTIVE_ACTIVE,
                'value'     => 'smtp.yandex.ru',
                'default'   => 'smtp.yandex.ru',
                'name'      => 'Хост почты',
                'type'      => 'string',
                'input_type'=> SmartInput::TYPE_TEXT,
            ]
        );

        $this->insert(
            $this->tableName,
            [
                'param'     => 'MAILER.USERNAME',
                'active' => \app\components\models\SmartRecord::ACTIVE_ACTIVE,
                'value'     => 'webmaster-amd',
                'default'   => 'webmaster-amd',
                'name'      => 'Логин почты',
                'type'      => 'string',
                'input_type'=> SmartInput::TYPE_TEXT,
            ]
        );

        $this->insert(
            $this->tableName,
            [
                'param'     => 'MAILER.PASSWORD',
                'active' => \app\components\models\SmartRecord::ACTIVE_ACTIVE,
                'value'     => 'Te3wcRhx3FPJ',
                'default'   => 'Te3wcRhx3FPJ',
                'name'      => 'Пороль почты',
                'type'      => 'string',
                'input_type'=> SmartInput::TYPE_TEXT,
            ]
        );

        $this->insert(
            $this->tableName,
            [
                'param'     => 'MAILER.PORT',
                'active' => \app\components\models\SmartRecord::ACTIVE_ACTIVE,
                'value'     => '465',
                'default'   => '465',
                'name'      => 'Порт почты',
                'type'      => 'string',
                'input_type'=> SmartInput::TYPE_TEXT,
            ]
        );

        $this->insert(
            $this->tableName,
            [
                'param'     => 'MAILER.ENCRYPTION',
                'active' => \app\components\models\SmartRecord::ACTIVE_ACTIVE,
                'value'     => 'ssl',
                'default'   => 'ssl',
                'name'      => 'Шифрование',
                'type'      => 'string',
                'input_type'=> SmartInput::TYPE_TEXT,
            ]
        );

        $this->insert(
            $this->tableName,
            [
                'param' => 'PARAM.IMAGE_FULL.MAX_WIDTH',
                'active' => \app\components\models\SmartRecord::ACTIVE_ACTIVE,
                'value' => '1280',
                'default' => '1280',
                'name'    => 'Максимальная ширина картинки',
                'type'    => 'string',
                'input_type' => SmartInput::TYPE_NUMBER,
            ]
        );

        $this->insert(
            $this->tableName,
            [
                'param' => 'PARAM.IMAGE_FULL.MAX_HEIGHT',
                'active' => \app\components\models\SmartRecord::ACTIVE_ACTIVE,
                'value' => '720',
                'default' => '720',
                'name'    => 'Максимальная высота картинки',
                'type'    => 'string',
                'input_type' => SmartInput::TYPE_NUMBER,
            ]
        );

        $this->insert(
            $this->tableName,
            [
                'param' => 'PARAM.IMAGE_THUMB.MAX_WIDTH',
                'active' => \app\components\models\SmartRecord::ACTIVE_ACTIVE,
                'value' => '400',
                'default' => '400',
                'name'    => 'Максимальная ширина картинки',
                'type'    => 'string',
                'input_type' => SmartInput::TYPE_NUMBER,
            ]
        );

        $this->insert(
            $this->tableName,
            [
                'param' => 'PARAM.IMAGE_THUMB.MAX_HEIGHT',
                'active' => \app\components\models\SmartRecord::ACTIVE_ACTIVE,
                'value' => '300',
                'default' => '300',
                'name'    => 'Максимальная высота картинки',
                'type'    => 'string',
                'input_type' => SmartInput::TYPE_NUMBER,
            ]
        );

        $this->insert(
            $this->tableName,
            [
                'param' => 'PARAM.FILE.MAX_SIZE',
                'active' => \app\components\models\SmartRecord::ACTIVE_ACTIVE,
                'value' =>  10 * 1024 * 1024,
                'default' => 10 * 1024 * 1024,
                'name'    => 'Максимальный размер файла в кБ',
                'type'    => 'string',
                'input_type' => SmartInput::TYPE_NUMBER,
            ]
        );

        $this->insert(
            $this->tableName,
            [
                'param' => 'PARAM.SITE.LANGUAGE',
                'active' => \app\components\models\SmartRecord::ACTIVE_ACTIVE,
                'value' =>  0,
                'default' => 0,
                'name'    => 'Язык сайта',
                'type'    => 'select',
                'input_type' => SmartInput::TYPE_SELECT,
            ]
        );

        $this->insert(
            $this->tableName,
            [
                'param' => 'PARAM.GUS.REVERS',
                'active' => \app\components\models\SmartRecord::ACTIVE_ACTIVE,
                'value' =>  0,
                'default' => 0,
                'name'    => 'Обратные URL Гуся',
                'type'    => 'boolean',
                'input_type' => SmartInput::TYPE_CHECKBOX,
            ]
        );

        $this->insert(
            $this->tableName,
            [
                'param' => 'PARAM.GUS.MIN_ITEMS',
                'active' => \app\components\models\SmartRecord::ACTIVE_ACTIVE,
                'value' =>  5,
                'default' => 5,
                'name'    => 'Минимальный размер соты гуся Гуся',
                'type'    => 'string',
                'input_type' => SmartInput::TYPE_NUMBER,
            ]
        );

        $this->insert(
            $this->tableName,
            [
                'param' => 'PARAM.GUS.MAX_ITEMS',
                'active' => \app\components\models\SmartRecord::ACTIVE_ACTIVE,
                'value' =>  10,
                'default' => 10,
                'name'    => 'Максимальный размер соты гуся Гуся',
                'type'    => 'string',
                'input_type' => SmartInput::TYPE_NUMBER,
            ]
        );

        // Языки

        $languages = [
            [
                'name' => 'Азербайджанский',
                'code' => 'az_AZ',
                'code_yandex' => 'az',
            ],
            [
                'name' => 'Албанский',
                'code' => 'sq_SQ',
                'code_yandex' => 'sq',
            ],
            [
                'name' => 'Английский',
                'code' => 'en_EN',
                'code_yandex' => 'en',
            ],
            [
                'name' => 'Арабский',
                'code' => 'ar_AR',
                'code_yandex' => 'ar',
            ],
            [
                'name' => 'Армянский',
                'code' => 'hy_HY',
                'code_yandex' => 'hy',
            ],
            [
                'name' => 'Африкаанс',
                'code' => 'af_AF',
                'code_yandex' => 'af',
            ],
            [
                'name' => 'Баскский',
                'code' => 'eu_EU',
                'code_yandex' => 'eu',
            ],
            [
                'name' => 'Башкирский',
                'code' => 'ba_BA',
                'code_yandex' => 'ba',
            ],
            [
                'name' => 'Белорусский',
                'code' => 'be_BE',
                'code_yandex' => 'be',
            ],
            [
                'name' => 'Болгарский',
                'code' => 'bg_BG',
                'code_yandex' => 'bg',
            ],
            [
                'name' => 'Боснийский',
                'code' => 'bs_BS',
                'code_yandex' => 'bs',
            ],
            [
                'name' => 'Валлийский',
                'code' => 'cy_CY',
                'code_yandex' => 'cy',
            ],
            [
                'name' => 'Венгерский',
                'code' => 'hu_HU',
                'code_yandex' => 'hu',
            ],
            [
                'name' => 'Вьетнамский',
                'code' => 'vi_VI',
                'code_yandex' => 'vi',
            ],
            [
                'name' => 'Гаитянский',
                'code' => 'ht_HT',
                'code_yandex' => 'ht',
            ],
            [
                'name' => 'Галисийский',
                'code' => 'gl_GL',
                'code_yandex' => 'gl',
            ],
            [
                'name' => 'Голландский',
                'code' => 'nl_NL',
                'code_yandex' => 'nl',
            ],
            [
                'name' => 'Греческий',
                'code' => 'el_EL',
                'code_yandex' => 'el',
            ],
            [
                'name' => 'Грузинский',
                'code' => 'ka_KA',
                'code_yandex' => 'ka',
            ],
            [
                'name' => 'Датский',
                'code' => 'da_DA',
                'code_yandex' => 'da',
            ],
            [
                'name' => 'Иврит',
                'code' => 'he_HE',
                'code_yandex' => 'he',
            ],
            [
                'name' => 'Индонезийский',
                'code' => 'id_ID',
                'code_yandex' => 'id',
            ],
            [
                'name' => 'Ирландский',
                'code' => 'ga_GA',
                'code_yandex' => 'ga',
            ],
            [
                'name' => 'Исландский',
                'code' => 'is_IS',
                'code_yandex' => 'is',
            ],
            [
                'name' => 'Испанский',
                'code' => 'es_ES',
                'code_yandex' => 'es',
            ],
            [
                'name' => 'Итальянский',
                'code' => 'it_IT',
                'code_yandex' => 'it',
            ],
            [
                'name' => 'Казахский',
                'code' => 'kk_KK',
                'code_yandex' => 'kk',
            ],
            [
                'name' => 'Каталанский',
                'code' => 'ca_CA',
                'code_yandex' => 'ca',
            ],
            [
                'name' => 'Киргизский',
                'code' => 'ky_KY',
                'code_yandex' => 'ky',
            ],
            [
                'name' => 'Китайский',
                'code' => 'zh_ZH',
                'code_yandex' => 'zh',
            ],
            [
                'name' => 'Корейский',
                'code' => 'ko_KO',
                'code_yandex' => 'ko',
            ],
            [
                'name' => 'Латынь',
                'code' => 'la_LA',
                'code_yandex' => 'la',
            ],
            [
                'name' => 'Латышский',
                'code' => 'lv_LV',
                'code_yandex' => 'lv',
            ],
            [
                'name' => 'Литовский',
                'code' => 'lt_LT',
                'code_yandex' => 'lt',
            ],
            [
                'name' => 'Македонский',
                'code' => 'mk_MK',
                'code_yandex' => 'mk',
            ],
            [
                'name' => 'Малагасийский',
                'code' => 'mg_MG',
                'code_yandex' => 'mg',
            ],
            [
                'name' => 'Малайский',
                'code' => 'ms_MS',
                'code_yandex' => 'ms',
            ],
            [
                'name' => 'Мальтийский',
                'code' => 'mt_MT',
                'code_yandex' => 'mt',
            ],
            [
                'name' => 'Монгольский',
                'code' => 'mn_MN',
                'code_yandex' => 'mn',
            ],
            [
                'name' => 'Немецкий',
                'code' => 'de_DE',
                'code_yandex' => 'de',
            ],
            [
                'name' => 'Норвежский',
                'code' => 'no_NO',
                'code_yandex' => 'no',
            ],
            [
                'name' => 'Персидский',
                'code' => 'fa_FA',
                'code_yandex' => 'fa',
            ],
            [
                'name' => 'Польский',
                'code' => 'pl_PL',
                'code_yandex' => 'pl',
            ],
            [
                'name' => 'Португальский',
                'code' => 'pt_PT',
                'code_yandex' => 'pt',
            ],
            [
                'name' => 'Румынский',
                'code' => 'ro_RO',
                'code_yandex' => 'ro',
            ],
            [
                'name' => 'Русский',
                'code' => 'ru_RU',
                'code_yandex' => 'ru',
            ],
            [
                'name' => 'Сербский',
                'code' => 'sr_SR',
                'code_yandex' => 'sr',
            ],
            [
                'name' => 'Словацкий',
                'code' => 'sk_SK',
                'code_yandex' => 'sk',
            ],
            [
                'name' => 'Словенский',
                'code' => 'sl_SL',
                'code_yandex' => 'sl',
            ],
            [
                'name' => 'Суахили',
                'code' => 'sw_SW',
                'code_yandex' => 'sw',
            ],
            [
                'name' => 'Тагальский',
                'code' => 'tl_TL',
                'code_yandex' => 'tl',
            ],
            [
                'name' => 'Таджикский',
                'code' => 'tg_TG',
                'code_yandex' => 'tg',
            ],
            [
                'name' => 'Тайский',
                'code' => 'th_TH',
                'code_yandex' => 'th',
            ],
            [
                'name' => 'Татарский',
                'code' => 'tt_TT',
                'code_yandex' => 'tt',
            ],
            [
                'name' => 'Турецкий',
                'code' => 'tr_TR',
                'code_yandex' => 'tr',
            ],
            [
                'name' => 'Узбекский',
                'code' => 'uz_UZ',
                'code_yandex' => 'uz',
            ],
            [
                'name' => 'Украинский',
                'code' => 'uk_UK',
                'code_yandex' => 'uk',
            ],
            [
                'name' => 'Урду',
                'code' => 'ur_UR',
                'code_yandex' => 'ur',
            ],
            [
                'name' => 'Финский',
                'code' => 'fi_FI',
                'code_yandex' => 'fi',
            ],
            [
                'name' => 'Французский',
                'code' => 'fr_FR',
                'code_yandex' => 'fr',
            ],
            [
                'name' => 'Хорватский',
                'code' => 'hr_HR',
                'code_yandex' => 'hr',
            ],
            [
                'name' => 'Хинди',
                'code' => 'hi_HI',
                'code_yandex' => 'hi',
            ],
            [
                'name' => 'Чешский',
                'code' => 'cs_CS',
                'code_yandex' => 'cs',
            ],
            [
                'name' => 'Шведский',
                'code' => 'sv_SV',
                'code_yandex' => 'sv',
            ],
            [
                'name' => 'Эльфийский (Синдарин)',
                'code' => 'sjn_SJN',
                'code_yandex' => 'sjn',
            ],
            [
                'name' => 'Эстонский',
                'code' => 'et_ET',
                'code_yandex' => 'et',
            ],
            [
                'name' => 'Японский',
                'code' => 'ja_JA',
                'code_yandex' => 'ja',
            ],
        ];

        foreach ($languages as $language) {
            $this->insert(
                $this->languageTableName,
                $language
            );
        }
    }

    public function down()
    {
        $this->dropTable($this->tableName);
        $this->dropTable($this->languageTableName);
    }
}
