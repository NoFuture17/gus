<?php

namespace app\migrations\tpl;
use app\migrations\generators\GalleryItem;
use yii\db\Schema;
use yii\db\Migration;
use \app\migrations\generators\Page as PageGenerator;

class create_news extends \app\migrations\Migration
{
    public $newsTableName = '{{%news}}';
    public $newsCategoryTableName = '{{%news_category}}';
    public $newsImageTableName = '{{%news_image_gallery}}';
    public $newsVideoTableName = '{{%news_video_gallery}}';


    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        // Создание таблицы новостей
        $newsGenerator = new PageGenerator($this, $this->newsTableName);
        $newsGenerator->additionalColumns['id_category'] = $this->integer();
        $newsGenerator->addIndex('id_category');
        $newsGenerator->create();

        // Создание таблицы категорий новостей
        $newsCategoryGenerator = new PageGenerator($this, $this->newsCategoryTableName);
        $newsCategoryGenerator->additionalColumns['id_parent'] = $this->integer();
        $newsCategoryGenerator->additionalColumns['lft'] = $this->integer();
        $newsCategoryGenerator->additionalColumns['rgt'] = $this->integer();
        $newsCategoryGenerator->additionalColumns['depth'] = $this->integer();
        $newsCategoryGenerator->additionalColumns['tree'] = $this->integer();
        $newsCategoryGenerator->addIndex('lft');
        $newsCategoryGenerator->addIndex('rgt');
        $newsCategoryGenerator->addIndex('depth');
        $newsCategoryGenerator->addIndex('tree');
        $newsCategoryGenerator->addIndex('id_parent');
        $newsCategoryGenerator->create();

        // Создание таблицы галереии изображений новостей
        $newsImage = new GalleryItem($this, $this->newsImageTableName);
        $newsImage->create();

        // Создание таблицы галереии изображений новостей
        $newsImage = new GalleryItem($this, $this->newsVideoTableName);
        $newsImage->create();
    }

    public function safeDown()
    {
        $this->dropTable($this->newsTableName);
        $this->dropTable($this->newsCategoryTableName);
        $this->dropTable($this->newsImageTableName);
    }

}
