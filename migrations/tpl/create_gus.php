<?php
/**
 * Created by PhpStorm.
 * User: nofuture17
 * Date: 25.02.16
 * Time: 11:42
 */

namespace app\migrations\tpl;


class create_gus extends \app\migrations\Migration
{
    public $soteTableName = "{{%sote}}";

    public function up()
    {
        // Создание таблицы страниц
        $pageGenerator = new \app\migrations\generators\Page($this, $this->soteTableName);
        $pageGenerator->additionalColumns['lft'] = $this->integer();
        $pageGenerator->additionalColumns['rgt'] = $this->integer();
        $pageGenerator->additionalColumns['depth'] = $this->integer();
//        $pageGenerator->additionalColumns['tree'] = $this->integer();
        $pageGenerator->additionalColumns['id_parent'] = $this->integer();
        $pageGenerator->addIndex('lft');
        $pageGenerator->addIndex('rgt');
        $pageGenerator->addIndex('depth');
//        $pageGenerator->addIndex('tree');
        $pageGenerator->addIndex('id_parent');
        $pageGenerator->create();
    }

    public function down()
    {
        $this->dropTable($this->soteTableName);
    }
}