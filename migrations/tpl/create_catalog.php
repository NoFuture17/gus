<?php

use yii\db\Schema;
use yii\db\Migration;
use \app\migrations\generators;

class create_catalog extends app\migrations\Migration
{
    public $catalogItemTableName        = '{{%catalog_item}}';
    public $catalogTagTableName         = '{{%catalog_tag}}';
    public $catalogCategoryTableName    = '{{%catalog_category}}';
    public $catalogItemOptionTable      = '{{%catalog_item_option}}';
    public $catalogItemOptionValueTable = '{{%catalog_item_option_value}}';
    public $catalogItemFileTableName    = '{{%catalog_item_file}}';
    public $catalogItemImageTableName   = '{{%catalog_item_image}}';

    /**/
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        // Создание таблицы элементов каталога
        $catalogItemGenerator = new generators\Page($this, $this->catalogItemTableName);
        $catalogItemGenerator->additionalColumns['id_category'] = $this->integer()->notNull();
        $catalogItemGenerator->addIndex('id_category');
        $catalogItemGenerator->create();

        // Создание таблицы категорий каталога
        $catalogCategoryGenerator = new generators\Page($this, $this->catalogCategoryTableName);
        $catalogCategoryGenerator->additionalColumns['id_parent'] = $this->integer()->defaultValue(0)->notNull();
        $catalogCategoryGenerator->addIndex('id_parent');
        $catalogCategoryGenerator->create();

        // Создание таблицы тегов каталога
        $catalogTagGenerator = new generators\Page($this, $this->catalogTagTableName);
        $catalogTagGenerator->create();

        // Создаём таблицу связей элементов каталога и тегов каталога
        // Задаём доп поле спецразмещения
        $additionalColumns = [
            'special' => $this->smallInteger()->notNull()
        ];
        $relationItemToTagTableName = $this->getRelationTableName($this->catalogItemTableName, $this->catalogTagTableName);
        $additionalIndexes = [
            'special' => $this->getIndexTemplate($relationItemToTagTableName, 'special')
        ];
        $this->createRelationTable(
            $this->catalogItemTableName,
            'id_item',
            $this->catalogTagTableName,
            'id_tag',
            $additionalColumns,
            $additionalIndexes,
            $this->tableOptions
        );

        //Создание таблиц опций элемента каталога
        $optionGenerator = new generators\Option($this, $this->catalogItemOptionTable);
        $optionGenerator->create();
        $optionValueGenerator = new generators\OptionValue($this, $this->catalogItemOptionValueTable);
        $optionValueGenerator->create();


        // Создаём таблицу связи опций элементов и категорий каталога
        $this->createRelationTable(
            $this->catalogItemOptionTable,
            'id_option',
            $this->catalogCategoryTableName,
            'id_category',
            null,
            null,
            $this->tableOptions
        );

        // Создём таблицу файлов
        $fileGenerator = new generators\File($this, $this->catalogItemFileTableName);
        $fileGenerator->create();

        // Создём таблицу картинок
        $imageGenerator = new generators\File($this, $this->catalogItemImageTableName);
        $imageGenerator->create();
    }

    public function safeDown()
    {
        // Удаляем таблицы
        $this->dropTable($this->catalogItemTableName);
        $this->dropTable($this->catalogCategoryTableName);
        $this->dropTable($this->catalogTagTableName);
        $optionToCategoryTableName = $this->getRelationTableName($this->catalogItemTableName, $this->catalogTagTableName);
        $this->dropTable($optionToCategoryTableName);
        $this->dropTable($this->catalogItemFileTableName);
    }

}
