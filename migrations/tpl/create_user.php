<?php
namespace app\migrations\tpl;

use yii\db\Schema;
use yii\db\Migration;
use \app\modules\user\models\User as UserModel;
use \app\modules\user\models\Profile as UserProfile;
use \app\migrations\generators;

class create_user extends \app\migrations\Migration
{
    public $userTableName = '{{%user}}';
    public $profileTableName = '{{%user_profile}}';

    public function safeUp()
    {
        // Создаём таблицу пользователей
        $userGenerator = new generators\User($this, $this->userTableName);
        $userGenerator->create();

        // Создаём таблицу профилей пользователей
        $profileGenerator = new generators\Profile($this, $this->profileTableName);
        $profileGenerator->create();

        // Добавляем пользователя админ
        $user   = new UserModel();
        $user->username = 'admin';
        $user->email    = 'admin@admin.admin';
        $user->setPassword('admin');
        $user->active   = UserModel::ACTIVE_ACTIVE;
        $user->role     = UserModel::ROLE_ADMIN;
        $user->generateAuthKey();
        $user->save();

        // Добавляем профиль пользователю админ
        $profile = new UserProfile();
        $profile->id_user = $user->id;
        $profile->fullname = 'Админов Админ АдминЫЧ';
        $profile->phone_mobile = '8 800 555 35 35';
        $profile->save();
    }

    public function safeDown()
    {
        $this->dropTable($this->userTableName);
        $this->dropTable($this->profileTableName);
    }
}
