<?php

class m160315_043824_create_parser_task extends \app\migrations\Migration
{
    public $tableName = '{{%parser_task}}';

    public function up()
    {
        $columns = [
            'id'                => $this->primaryKey(),
            'active'            => $this->smallInteger(),
            'name'              => $this->string(255),
            'result_count'      => $this->integer(),
            'data'              => $this->text(),
        ];

        $generator = new \app\migrations\generators\Generator($this, $this->tableName);
        $generator->additionalColumns = $columns;
        $generator->create();
    }

    public function down()
    {
        $this->dropTable('parser_task');
    }
}
