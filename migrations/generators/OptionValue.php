<?php
/**
 * Created by PhpStorm.
 * User: nofuture17
 * Date: 20.11.15
 * Time: 11:49
 */

namespace app\migrations\generators;


class OptionValue extends Generator
{
    /**
     * Возвращает шаблон колонок таблицы значений опций
     * @return array
     */
    public function getColumnsTemplate() {
        $columns = [
            'id'        => $this->_migration->primaryKey(),
            'id_option' => $this->_migration->smallInteger(),
            'id_item'   => $this->_migration->smallInteger(),
            'value'     => $this->_migration->text(),
        ];

        return $columns;
    }

    /**
     * Возвращает шаблон индексов таблицы значений опций
     * @return array
     */
    public function getIndexesTemplate() {
        $indexes = [
            'id_option' => $this->_migration->getIndexTemplate($this->tableName, 'id_option', ['unique' => true]),
            'id_item' => $this->_migration->getIndexTemplate($this->tableName, 'id_item', ['unique' => true]),
        ];

        return $indexes;
    }
}