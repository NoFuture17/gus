<?php
/**
 * Created by PhpStorm.
 * User: nofuture17
 * Date: 20.11.15
 * Time: 11:44
 */

namespace app\migrations\generators;
use \app\migrations\Migration;

class Generator
{
    public $tableName;
    public $primaryKey = 'id';
    public $additionalColumns = [];
    public $additionsIndexes = [];

    /**
     * @var \app\migrations\Migration;
     */
    protected $_migration;
    protected $_columns;
    protected $_indexes;

    public function __construct(Migration $migration, $tableName)
    {
        $this->_migration   = $migration;
        $this->tableName    = $tableName;
    }

    /**
     * Создаёт таблицу и индексы вбазе данных
     */
    public function create() {
        $columns = \yii\helpers\ArrayHelper::merge($this->additionalColumns, $this->getColumnsTemplate());
        $indexes = \yii\helpers\ArrayHelper::merge($this->additionsIndexes, $this->getIndexesTemplate());
        $this->_migration->createTable($this->tableName, $columns, $this->_migration->tableOptions);
        $this->_migration->createIndexes($indexes);
    }

    /**
     * Добавляет дополнительный индекс
     * @param string $indexName
     * @param array|null $params
     */
    public function addIndex($indexName, $params = null) {
        $this->additionsIndexes[$indexName] = $this->_migration->getIndexTemplate($this->tableName, $indexName, $params);
    }

    /**
     * Возвращает шаблон колонок таблицы
     * @return array
     */
    public function getColumnsTemplate() {
        $columns = [
        ];

        return $columns;
    }

    /**
     * Возвращает шаблон индексов таблицы
     * @return array
     */
    public function getIndexesTemplate() {
        $indexes = [
        ];

        return $indexes;
    }
}