<?php
/**
 * Created by PhpStorm.
 * User: nofuture17
 * Date: 01.02.16
 * Time: 18:41
 */

namespace app\migrations\generators;


class Language extends Generator
{
    /**
     * Возвращает шаблон колонок таблицы файлов
     * @return array
     */
    public function getColumnsTemplate() {
        $columns = [
            'id'                => $this->_migration->primaryKey(),
            'name'              => $this->_migration->string(255),
            'code'              => $this->_migration->string(32),
            'code_yandex'       => $this->_migration->string(32),
        ];

        return $columns;
    }

    /**
     * Возвращает шаблон индексов таблицы файлов
     * @return array
     */
    public function getIndexesTemplate() {
        $indexes = [
        ];

        return $indexes;
    }
}