<?php
/**
 * Created by PhpStorm.
 * User: nofuture17
 * Date: 20.11.15
 * Time: 14:47
 */

namespace app\migrations\generators;


class Profile extends Generator
{
    /**
     * Возвращает шаблон колонок таблицы профилей пользователей
     * @return array
     */
    public function getColumnsTemplate() {
        $columns = [
            'id'            => $this->_migration->primaryKey(),
            'id_user'       => $this->_migration->integer(),
            'active'        => $this->_migration->integer()->defaultValue(\app\components\models\SmartRecord::ACTIVE_BLOCKED),
            'fullname'      => $this->_migration->string(255),
            'phone_mobile'  => $this->_migration->string(255),
            'image_full'    => $this->_migration->string(255),
            'image_small'   => $this->_migration->string(255),
        ];

        return $columns;
    }

    /**
     * Возвращает шаблон индексов таблицы профилей пользователей
     * @return array
     */
    public function getIndexesTemplate() {
        $indexes = [
            'id_user' => $this->_migration->getIndexTemplate($this->tableName, 'id_user', ['unique' => true])
        ];

        return $indexes;
    }
}