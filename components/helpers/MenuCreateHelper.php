<?php
/**
 * User: lagprophet
 * Date: 15.12.15
 * Time: 18:39
 */

namespace app\components\helpers;


use app\modules\news\models\NewsCategory;

class MenuCreateHelper
{

    public static function processCategory($category)
    {
        return ['label' => $category->name];
    }

    /**
     * Создает массив для \yii\widgets\Menu
     *
     * Метод находит все дочерние категории и формирует массив.
     * Функция работает рекурсивно.
     *
     * @param \app\components\models\Page[] $categories
     * @return array
     */
    public static function menuFromCategoryWithChildren($categories)
    {
        $result = [];
        foreach ($categories as $category) {
            $item = self::processCategory($category);
            $children = NewsCategory::find()->where(['id_parent' => $category->id])->all();
            $item += self::menuFromCategoryWithChildren($children);
            $result['items'] = $item;
        }
        return $result;
    }
}