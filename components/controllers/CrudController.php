<?php
/**
 * Created by PhpStorm.
 * User: nofuture17
 * Date: 16.12.15
 * Time: 15:04
 */

namespace app\components\controllers;

/**
 * Class CrudController
 * Базовый класс CRUD контроллера
 * @package app\components\controllers
 */
abstract class CrudController extends Controller
{

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => \yii\filters\VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Создаёт новую модель
     * @param \app\components\models\SmartModel $model
     * @return bool результат сохранения модели
     */
    protected function createModel($model)
    {
        if ($model->load(\Yii::$app->request->post())) {
            return $model->save();
        }

        return false;
    }

    /**
     * Обновляет модель
     * @param \app\components\models\SmartModel $model
     * @return bool результат сохранения модели
     */
    protected function updateModel($model)
    {
        if ($model->load(\Yii::$app->request->post())) {
            return $model->save();
        }

        return false;
    }

    /**
     * Обновляет модель черех ajax, отправляет отчет пользователю и выключает перезапись связанных данных
     * @param \app\components\models\SmartModel $model
     * @return void
     */
    public function updateModelAjax($model)
    {
        $model->relationChange = false;

        if ($this->updateModel($model)) {
            $modelName = (!empty($model->name)) ? $model->name : $model->primaryKey;
            \app\components\helpers\AjaxHelper::echoReport('success', ["Изменения в `{$modelName}` сохранены."]);
        } else {
            // Если данные пришли, но не валидны
            if (!empty($model->getErrors())) {
                \app\components\helpers\AjaxHelper::echoReport('error', $model->getErrors());
            } else {
                \app\components\helpers\AjaxHelper::echoReport('error', ['Что-то не так!']);
            }
        }
    }
}