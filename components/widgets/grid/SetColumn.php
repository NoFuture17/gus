<?php
/**
 * Created by PhpStorm.
 * User: nofuture17
 * Date: 25.11.15
 * Time: 9:32
 */

namespace app\components\widgets\grid;

use yii\grid\DataColumn;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use \app\components\widgets\form\SmartInput;

class SetColumn extends DataColumn
{
    public $inputData;
    public $formAction;
    /*
     * Устанавливать фильтр из модели
     */
    public $setFilter = false;

    public function init()
    {
        parent::init();

        $model = $this->grid->filterModel;
        // Устанавливает фильтр из модели
        if (empty($this->filter) && $this->setFilter ) {
            if ($model instanceof \app\components\interfaces\SmartInputInterface && $filterData = $model->getInputData($this->attribute)) {
                $this->filter = $filterData;
            }
        }
    }

    /**
     * @param \app\models\Page $model
     * @param mixed $key
     * @param int $index
     * @return string
     */
    protected function renderDataCellContent($model, $key, $index)
    {
        //$value = $this->getDataCellValue($model, $key, $index);

         $smartInput = new SmartInput([
            'model' => $model,
            'attribute' => $this->attribute,
            'data' => $this->inputData,
            'options' => [
                'htmlOptions' => [
                    'class' => 'form-control',
                    'data-ajax-action' => 'online-change',
                    'data-form-action' => $this->_formAction($model)
                ]
            ]
        ]);

        return $smartInput->run();
    }

    protected function _formAction($model) {
        if (empty($this->formAction) && empty($this->formAction['route']))
            return false;

        $params = [$this->formAction['route']];
        if (!empty($this->formAction['params'])) {
            foreach ($this->formAction['params'] as $key => $value) {
                if ($value[0] == ':' && !empty($model->{substr($value, 1)})) {
                    $params[$key] = $model->{substr($value, 1)};
                } else {
                    $params[$key] = $value;
                }
            }
        }

        return Url::to($params);
    }
}