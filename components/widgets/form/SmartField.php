<?php
/**
 * Created by PhpStorm.
 * User: nofuture17
 * Date: 09.12.15
 * Time: 19:37
 */

namespace app\components\widgets\form;
use \yii\helpers\Html;

class SmartField extends \yii\widgets\ActiveField
{
    public function singleImageInput($options = [])
    {
        // https://github.com/yiisoft/yii2/pull/795
        if ($this->inputOptions !== ['class' => 'form-control']) {
            $options = array_merge($this->inputOptions, $options);
        }
        $this->adjustLabelFor($options);
        $this->parts['{input}'] = '';

        if (!empty($this->model->{$this->attribute})) {
            $this->parts['{input}'] .= Html::tag(
                'div',
                Html::tag(
                    'div',
                    Html::a(
                        Html::img($this->model->{$this->attribute}, [
                            'class' => 'img-thumbnail',
                            'alt'   => $this->attribute,
                            'title' => $this->attribute,
                        ]),
                        $this->model->{$this->attribute},
                        ['class' => 'thumbnail thumbnail-single']
                    ),
                    ['class' => 'col-xs-6 col-md-3']
                ),
                ['class' => 'row']
            );
        }

        $this->parts['{input}'] .= Html::activeFileInput($this->model, $this->attribute, $options);

        return $this;
    }

    public function chosenSelect($items, $options = [])
    {
        $options['htmlOptions']['data-chosen'] = true;
        if (!empty($options['multiple'])) {
            $options['htmlOptions']['multiple'] = $options['multiple'];
        }
        $this->parts['{input}'] = Html::activeDropDownList($this->model, $this->attribute, $items, $options['htmlOptions']);
    }

}