<?php
/**
 * User: lagprophet
 * Date: 14.12.15
 * Time: 16:37
 */

namespace app\components\widgets\form\SmartTable;


class SmartTable extends \yii\base\Widget
{
    public $model;
    public $attribute;

    /**
     * @var \app\components\behaviors\CustomObjectsFieldBehavior
     */
    public $dataManager;

    /**
     * @inheritdoc
     */
    public function run($return = false)
    {

        return $this->renderFile('@app/components/widgets/form/SmartTable/views/table.php',
            [
                /**
                 * Список полей объекта
                 * @var array
                 */
                'fieldsList' => $this->dataManager->objectAttributes,
                /**
                 * Массив объектов
                 * @var array
                 */
                'objects'   => $this->dataManager->getObjects(),
                'model'     => $this->model,
                'attribute' => $this->attribute,
                'newObject' => $this->dataManager->getObject(),
            ]
        );
    }
}