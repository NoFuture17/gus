<?php
/**
 * User: lagprophet
 * Date: 28.12.15
 * Time: 15:23
 */

namespace app\components\widgets\form;


use yii\widgets\ActiveForm;

/**
 * Class DummyActiveForm
 * Форма, которая не выводит себя где не надо, в отличии от пса-родителя
 * @package app\components\widgets\form
 */
class DummyActiveForm extends ActiveForm
{
    public function init()
    {
        if (!isset($this->options['id'])) {
            $this->options['id'] = $this->getId();
        }
    }
}