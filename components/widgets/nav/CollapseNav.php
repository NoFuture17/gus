<?php
/**
 * Created by PhpStorm.
 * User: nofuture17
 * Date: 17.12.15
 * Time: 11:18
 */

namespace app\components\widgets\nav;

use Yii;
use yii\base\InvalidConfigException;
use yii\base\Widget;
use yii\bootstrap\Dropdown;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;


/**
 * Nav renders a nav HTML component.
 *
 * For example:
 *
 * ```php
 * echo Nav::widget([
 *     'items' => [
 *         [
 *             'label' => 'Home',
 *             'url' => ['site/index'],
 *             'linkOptions' => [...],
 *         ],
 *         [
 *             'label' => 'Dropdown',
 *             'items' => [
 *                  ['label' => 'Level 1 - Dropdown A', 'url' => '#'],
 *                  '<li class="divider"></li>',
 *                  '<li class="dropdown-header">Dropdown Header</li>',
 *                  ['label' => 'Level 1 - Dropdown B', 'url' => '#'],
 *             ],
 *         ],
 *         [
 *             'label' => 'Login',
 *             'url' => ['site/login'],
 *             'visible' => Yii::$app->user->isGuest
 *         ],
 *     ],
 *     'options' => ['class' =>'nav-pills'], // set this to nav-tab to get tab-styled navigation
 * ]);
 * ```
 *
 * Note: Multilevel dropdowns beyond Level 1 are not supported in Bootstrap 3.
 *
 * @see http://getbootstrap.com/components/#dropdowns
 * @see http://getbootstrap.com/components/#nav
 *
 * @author Antonio Ramirez <amigo.cobos@gmail.com>
 * @since 2.0
 */
class CollapseNav extends \yii\bootstrap\Nav
{
    public $nestedItemCounter = 0;

    /**
     * Renders widget items.
     */
    public function renderItems($items = null)
    {
        if (empty($items)) {
            $items = $this->items;
        }
        $result = [];
        foreach ($items as $i => $item) {
            if (isset($item['visible']) && !$item['visible']) {
                continue;
            }
            $result[] = $this->renderItem($item);
        }

        return Html::tag('ul', implode("\n", $result), $this->options);
    }

    /**
     * Renders a widget's item.
     * @param string|array $item the item to render.
     * @return string the rendering result.
     * @throws InvalidConfigException
     */
    public function renderItem($item)
    {
        if (is_string($item)) {
            return $item;
        }
        if (!isset($item['label'])) {
            throw new InvalidConfigException("The 'label' option is required.");
        }
        $encodeLabel = isset($item['encode']) ? $item['encode'] : $this->encodeLabels;
        $label = $encodeLabel ? Html::encode($item['label']) : $item['label'];
        $options = ArrayHelper::getValue($item, 'options', []);
        $items = ArrayHelper::getValue($item, 'items');
        $url = ArrayHelper::getValue($item, 'url', '#');
        $linkOptions = ArrayHelper::getValue($item, 'linkOptions', []);

        if (isset($item['active'])) {
            $active = ArrayHelper::remove($item, 'active', false);
        } else {
            $active = $this->isItemActive($item);
        }

        if ($items !== null) {
            $linkOptions['data-toggle'] = 'collapse';
            //Html::addCssClass($options, ['widget' => 'dropdown']);
            //Html::addCssClass($linkOptions, ['widget' => 'dropdown-toggle']);
            if ($this->dropDownCaret !== '') {
                $label .= ' ' . $this->dropDownCaret;
            }
            if (is_array($items)) {
                if ($this->activateItems) {
                    $items = $this->isChildActive($items, $active);
                }
                Html::addCssClass($linkOptions, 'collapse');
                $itemsUlId = $this->id . '-' . ++$this->nestedItemCounter;
                $items = $this->renderDropdown($items, $itemsUlId);
                $url = '#' . $itemsUlId;
            }
        }

        if ($this->activateItems && $active) {
            Html::addCssClass($options, 'active');
        }

        return Html::tag('li', Html::a($label, $url, $linkOptions) . $items, $options);
    }

    /**
     * Renders the given items as a dropdown.
     * This method is called to create sub-menus.
     * @param array $items the given items. Please refer to [[Dropdown::items]] for the array structure.
     * @param array $parentItem the parent item information. Please refer to [[items]] for the structure of this array.
     * @return \yii\bootstrap\Dropdown
     * @since 2.0.1
     */
    protected function renderDropdown($items, $itemsUlId = null)
    {
        if ($itemsUlId == null) {
            $itemsUlId = $this->id . '-' . ++$this->nestedItemCounter;
        }

        $result = '';
        $result .= '<ul id="' . $itemsUlId . '" class="collapse">';

        foreach ($items as $item) {
            $result .= '<li>';

            if (!empty($item['items']) && count($item['items'])) {
                $url = '#' . $itemsUlId = $this->id . '-' . ++$this->nestedItemCounter;
                $result .= '<a href=" ' . $url . '" class="collapse" data-toggle="collapse"">' . $item['label'] . '<b class="caret"></b></a>';

                $result .= $this->renderDropdown($item['items'], $itemsUlId);

            } else {
                $result .= '<a href=" ' . Url::to($item['url']) . '">' . $item['label'] . '</a>';
            }
            $result .= '</li>';
        }

        $result .= '</ul>';

        return $result;
    }
}