<?php

namespace app\components\models\gallery;

use Yii;

/**
 * This is the model class for table "{{%news_image_gallery}}".
 *
 * @property integer $id
 * @property integer $id_item
 * @property integer $active
 * @property string $name
 * @property string $alt
 * @property string $content
 * @property string $text
 * @property integer $priority
 */
class GalleryItem extends \app\components\models\SmartRecord
{
    protected $formName;
    /**
     * Менеджер галереии
     * @var \app\components\behaviors\GalleryManager
     */
    public $galleryManager;

    /**
     * @inheritdoc
     */
    public function formName()
    {
        if (empty($this->formName)) {
            $id = $this->getIsNewRecord() ? md5(time() * rand(1, 100)) : $this->id;
            $this->formName = $this->galleryManager->galleryItemFormName . "[{$id}]";
        }

        return $this->formName;
    }

    /**
     * @inheritdoc
     */
//    public static function tableName()
//    {
//        return '{{%news_image_gallery}}';
//    }

    // Методы SmartInputInterface
    public function getInputType($attribute)
    {
        $result = null;

        switch ($attribute) {
            case 'id_item':
            case 'id':
                $result = \app\components\widgets\form\SmartInput::TYPE_HIDDEN;
                break;
            default:
                $result = parent::getInputType($attribute);
        }

        return $result;
    }

    public function getInputData($attribute)
    {
        $result = null;

        switch ($attribute) {
            default:
                $result = parent::getInputData($attribute);
        }

        return $result;
    }

    public function getInputOptions($attribute)
    {
        return parent::getInputOptions($attribute);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(
            parent::rules(),
            [
                [['id_item', 'priority'], 'integer'],
                [['text', 'content'], 'safe'],
                [['name', 'alt', 'id'], 'string', 'max' => 255]
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
            parent::attributeLabels(),
            [
                'id_item' => 'Id Item',
                'name' => 'Имя (title)',
                'alt' => 'Alt',
                'content' => 'Содержимое',
                'text' => 'Описание',
                'priority' => 'Приоритет',
            ]
        );
    }

    /**
     * @inheritdoc
     * @return GalleryItemQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new GalleryItemQuery(get_called_class());
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($insert) {
                $this->id = null;
            }
            return true;
        } else {
            return false;
        }
    }
}
