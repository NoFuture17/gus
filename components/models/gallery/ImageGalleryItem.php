<?php
/**
 * User: lagprophet
 * Date: 22.12.15
 * Time: 15:05
 */

namespace app\components\models\gallery;


class ImageGalleryItem extends GalleryItem
{

    public static function getGalleryType()
    {
        return \app\components\behaviors\GalleryManager::TYPE_IMAGE;
    }

    public function rules()
    {
        return array_merge(
            parent::rules(),
            []
        );
    }

    // Методы SmartInputInterface
    public function getInputType($attribute)
    {
        $result = null;

        switch ($attribute) {
            default:
                $result = parent::getInputType($attribute);
        }

        return $result;
    }

    public function getInputData($attribute)
    {
        $result = null;

        switch ($attribute) {
            default:
                $result = parent::getInputData($attribute);
        }

        return $result;
    }

    public function getInputOptions($attribute)
    {
        return parent::getInputOptions($attribute);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
            parent::attributeLabels(),
            [
                'content' => 'Изображение',
            ]
        );
    }

    /**
     * @inheritdoc
     * @return ImageGalleryItemQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ImageGalleryItemQuery(get_called_class());
    }

    /**
     * Удаляем картинку вместе с записью в базе
     * @return bool
     */
    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            unlink(\Yii::getAlias('@webroot' . $this->content));
            return true;
        } else {
            return false;
        }
    }

}