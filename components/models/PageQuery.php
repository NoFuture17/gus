<?php

namespace app\components\models;

/**
 * This is the ActiveQuery class for [[Page]].
 *
 * @see Page
 */
class PageQuery extends SmartQuery
{
    /**
     * Возвращает чистый объект запроса
     * @return \app\components\models\PageQuery
     */
    public function clean()
    {
        return parent::clean();
    }

    public function active()
    {
        $this->andWhere(['active' => Page::ACTIVE_ACTIVE]);
        return $this;
    }

    public function url($url) {
        $this->andWhere('url = :url', [':url' => $url]);
        return $this;
    }

    /**
     * Настраивает запрос для поля ввода
     * @return $this
     */
    public function toInput()
    {
        return $this->active();
    }

    /**
     * @inheritdoc
     * @return Page[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Page|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}