<?php
/**
 * Created by PhpStorm.
 * User: nofuture17
 * Date: 22.12.15
 * Time: 10:23
 */

namespace app\components\models;


class SmartRecord extends \yii\db\ActiveRecord implements \app\components\interfaces\SmartInputInterface
{
    const ACTIVE_BLOCKED    = -1;
    const ACTIVE_ACTIVE     = 1;

    /**
     * Флаг обновления связанных данных.
     *
     * Если установлен, данные будут обновлены.
     * @var bool
     */
    public $relationChange = true;

    public function getActiveName()
    {
        return ArrayHelper::getValue(static::getActiveArray(), $this->active);
    }

    public static function getActiveArray()
    {
        return [
            static::ACTIVE_BLOCKED    => 'Заблокирован',
            static::ACTIVE_ACTIVE     => 'Активен',
        ];
    }

    // Методы SmartInputInterface
    public function getInputType($attribute)
    {
        $result = null;

        switch ($attribute) {
            case 'active':
                $result = \app\components\widgets\form\SmartInput::TYPE_SELECT;
                break;
            default:
                $result = \app\components\widgets\form\SmartInput::TYPE_TEXT;
        }

        return $result;
    }

    public function getInputData($attribute)
    {
        $result = null;

        switch ($attribute) {
            case 'active' :
                $result = static::getActiveArray();
                break;
        }

        return $result;
    }

    public function getInputOptions($attribute)
    {
        $result = [
            'htmlOptions' => ['class' => 'form-control']
        ];

        if (!$this->isNewRecord) {
            $result['htmlOptions'] = \yii\helpers\ArrayHelper::merge(
                $result['htmlOptions'],
                ['data-model-pk' => $this->id]
            );
        }

        return $result;
    }

    public function rules()
    {
        return [
            ['active', 'integer'],
            ['active', 'default', 'value' => static::ACTIVE_BLOCKED],
            ['active', 'in', 'range' => array_keys(static::getActiveArray())],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'active' => 'Активность',
        ];
    }
}