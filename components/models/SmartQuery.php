<?php
/**
 * Created by PhpStorm.
 * User: nofuture17
 * Date: 22.12.15
 * Time: 10:24
 */

namespace app\components\models;


class SmartQuery extends \yii\db\ActiveQuery
{
    public $cache = true;

    public function noCache($off = true) {
        $this->cache = !$off;
        return $this;
    }

    public function byPk($id)
    {
        $this->andWhere('id = :id', [':id' => $id]);
        return $this;
    }

    public function active()
    {
        $this->andWhere(['active' => \app\components\models\SmartRecord::ACTIVE_ACTIVE]);
        return $this;
    }

    /**
     * Время жизни кеша по умолчанию
     * @var int
     */
    public static $cacheDuration = 3600;

    /**
     * Возвращает чистый объект запроса
     * @return \app\components\models\SmartQuery
     */
    public function clean()
    {
        $class = get_called_class();
        $query = new $class($this->modelClass);
        return $query;
    }

    /**
     * Зависимость кеша по умолчанию
     * @return \yii\caching\DbDependency|null
     */
    public static function getCacheDependency()
    {
        return null;
    }

    /**
     * @inheritdoc
     * @return Page[]|array|null
     */
    public function all($db = null)
    {
        if ($this->cache) {
            $db = \Yii::$app->db;
            return $db->cache(
                function($db){
                    return parent::all($db);
                }, static::$cacheDuration,
                static::getCacheDependency()
            );
        } else {
            return parent::all($db);
        }
    }

    /**
     * @inheritdoc
     * @return Page|array|null
     */
    public function one($db = null)
    {
        if ($this->cache) {
            $db = \Yii::$app->db;
            return $db->cache(
                function($db){
                    return parent::one($db);
                }, static::$cacheDuration,
                static::getCacheDependency()
            );
        } else {
            return parent::one($db);
        }
    }
}