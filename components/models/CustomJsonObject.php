<?php
/**
 * User: lagprophet
 * Date: 14.12.15
 * Time: 18:16
 */

namespace app\components\models;


class CustomJsonObject extends SmartModel
{
    public $code;

    public $baseFormName;

    /**
     * Порядковый номер объекта
     * @var int
     */
    public $serialIndex;

    /**
     * Объект для создания конфигурируемых полей модели
     * @var \app\components\behaviors\CustomObjectsFieldBehavior
     */
    public $objectManager;

    public function formName()
    {
        return $this->baseFormName . "[" . $this->serialIndex . "]";
    }

    /**
     * @inheritdoc
     */
    public function getInputType($attribute)
    {
        $result = null;

        switch ($attribute) {
            case 'code':
                $result = \app\components\widgets\form\SmartInput::TYPE_TEXTAREA;
                break;
            default:
                $result = parent::getInputType($attribute);
        }

        return $result;
    }

    /**
     * @inheritdoc
     */
    public function getInputData($attribute)
    {
        return parent::getInputData($attribute);
    }

    /**
     * @inheritdoc
     */
    public function getInputOptions($attribute)
    {
        return parent::getInputOptions($attribute);
    }
}