<?php
/**
 * Created by PhpStorm.
 * User: nofuture17
 * Date: 01.12.15
 * Time: 17:24
 */

namespace app\components\modules;

use Yii;
use \app\components\widgets\Menu;
/**
 * Базовый модуль.
 * Предусматривает и реализует функционал,
 * имеющийся у всех модулей приложения.
 *
 * @author nofuture17
 */
class Module extends \yii\base\Module implements \yii\base\BootstrapInterface {

    public function init()
    {
        parent::init();

        /**
         * Для консоли контроллеры искать в папке commands
         */
        if (Yii::$app instanceof ConsoleApplication) {
            $this->controllerNamespace = preg_replace(
                '/^(.*\\)controllers$/',
                '$1commands',
                $this->controllerNamespace
            );
        }
    }
    /**
     * Возвращает массив с меню для виджета \app\components\widgets\Menu
     * @return array
     */
    public static function getMenuItems() {
        return [];
    }

    protected static $_classPathAlias;

    /**
     * Выполняется пери инициализации приложения
     * @param type $app Экзепляр приложения
     */
    public function bootstrap($app)
    {
        // Регистрируем правила url для модуля
        $this->_registerUrlRules($app);
        $this->setMenuItems();
    }

    /**
     * Возвращает массив правил для обработки url
     *
     * return [
     *     [
     *         'rules' => [...rules[]...],
     *         'append' => true,
     *     ],
     * ];
     *
     * @return array
     */
    public function getUrlRules() {
        return [];
    }

    /**
     * добавляет пункты в меню
     */
    public function setMenuItems() {

        Menu::getInstance()->addItems(static::getMenuItems());

        foreach ($this->modules as $module) {
            Menu::getInstance()->addItems(call_user_func_array([$module['class'], 'self::getMenuItems'], []));
        }
    }

    /**
     * Регистрирует правила url для модуля
     * Правила берутся из функции getUrlRules
     * @param \yii\web\Application $app Экзепляр приложения
     */
    protected function _registerUrlRules($app) {
        $rules = $this->getUrlRules();
        if (!empty($rules)) {
            foreach ($rules as $rule) {
                if (!empty($rule))
                    $app->getUrlManager()->addRules($rule['rules'], ($rule['append'] === false) ? false : true);
            }
        }
    }
}
