<?php

namespace app\components\modules;

/**
 * Description of AdminModule
 *
 * @author nofuture17
 */
class Admin extends \app\components\modules\Module {
    public $layout      = '@app/views/layouts/admin';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
