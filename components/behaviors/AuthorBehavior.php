<?php
/**
 * Created by PhpStorm.
 * User: nofuture17
 * Date: 07.12.15
 * Time: 18:42
 */

namespace app\components\behaviors;
use \yii\db\ActiveRecord;

class AuthorBehavior extends \yii\base\Behavior
{
    /**
     * @inheritdoc
     */
    public function events()
    {
        return [
            ActiveRecord::EVENT_BEFORE_INSERT => 'beforeSave',
            ActiveRecord::EVENT_BEFORE_UPDATE => 'beforeSave',
        ];
    }

    public function beforeSave()
    {
        $this->owner->author = \Yii::$app->user->id;
    }

}