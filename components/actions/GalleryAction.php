<?php
/**
 * User: lagprophet
 * Date: 22.12.15
 * Time: 17:56
 */

namespace app\components\actions;


class GalleryAction extends \yii\base\Action
{

    /**
     * Класс модели галереи
     * @var string
     */
    public $modelClass;

    /**
     * Название класса поведения галереи
     * @var string
     */
    public $galleryManagerName;

    public function runWithParams()
    {
        $postData = \Yii::$app->request->post();
        $model = new $this->modelClass;
        $galleryManager = $model->getBehavior($this->galleryManagerName);
        $galleryItemModel = $galleryManager->getNewItem();

        $file = \yii\web\UploadedFile::getInstancesByName('file')[0];
        $newName = $galleryItemModel->id . '.' . $file->getExtension();
        $path = \Yii::getAlias('@webroot' . \Yii::$app->params['DATA_TEMP_DIR'] . $newName);
        $web  = \Yii::getAlias('@web' . \Yii::$app->params['DATA_TEMP_DIR'] . $newName);
        $file->saveAs($path);

        $galleryItemModel->content = $web;

        echo \app\components\widgets\form\gallery\GalleryItem::widget([
            'model' => $galleryItemModel,
            'type' => $postData['type']
        ]);
    }
}