<?php
/**
 * User: lagprophet
 * Date: 29.12.15
 * Time: 15:20
 */

namespace app\components\actions;


class VideoGalleryAction extends \yii\base\Action
{
    /**
     * Класс модели галереи
     * @var string
     */
    public $modelClass;

    /**
     * Название класса поведения галереи
     * @var string
     */
    public $galleryManagerName;

    public function runWithParams()
    {
        $postData = \Yii::$app->request->post();
        $model = new $this->modelClass;
        $galleryManager = $model->getBehavior($this->galleryManagerName);
        $galleryItemModel = $galleryManager->getNewItem();
        echo \app\components\widgets\form\gallery\GalleryItem::widget([
            'model' => $galleryItemModel,
            'type' => $postData['type']
        ]);
    }
}