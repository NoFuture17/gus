<?php
//namespace app\components\behaviors;
//use yii\base\Behavior;
//use yii\base\ErrorException;
//use yii\base\Object;
//use \yii\db\ActiveQuery
//use yii\db\ActiveRecord;
//use yii\helpers\ArrayHelper;
//use yii\db\ActiveRecordInterface;
///**
// * Saves Many-to-many relations
// */
//class RelationalBehavior extends Behavior
//{
//    /**
//     * Типы связей
//     */
//    const REL_HAS_ONE     = 'hasOne';
//    const REL_HAS_MANY    = 'hasMany';
//    const REL_MANY_MANY   = 'ManyMany';
//
//    /**
//     * @var ActiveRecord
//     */
//    public $owner;
//
//    public function attach($owner)
//    {
//        if (!($owner instanceof ActiveRecord)) {
//            throw new ErrorException('Owner must be instance of yii\db\ActiveRecord');
//        }
//        if (count($owner->getTableSchema()->primaryKey) > 1) {
//            throw new ErrorException('RelationalBehavior doesn\'t support composite primary keys');
//        }
//        parent::attach($owner);
//    }
//
//    public function canSetProperty($name, $checkVars = true)
//    {
//        $getter = 'get' . $name;
//        if (method_exists($this->owner, $getter) && $this->owner->$getter() instanceof ActiveQuery) {
//            return true;
//        }
//        return parent::canSetProperty($name, $checkVars);
//    }
//
//    /**
//     * Заменить старые данные связи, новыми
//     * @param string $name Имя связи
//     * @param int|int[]|ActiveRecord|ActiveRecord[]|array $data Данные
//     */
//    public function set($name, $data, $deletePrevious = false)
//    {
//
//    }
//
//    /**
//     * Добавить новые данные связи
//     * @param string $name Имя связи
//     * @param int|int[]|ActiveRecord|ActiveRecord[]|array $data Данные
//     */
//    public function push($name, $data)
//    {
//
//    }
//
//    /**
//     * Возвращает данные связи
//     * @param string $name Имя связи
//     * @param sting|array|null $condition Набор дополнительных условий для выборки (только множественные связи)
//     * @return ActiveRecord|ActiveRecord[]|null
//     */
//    public function get($name, $condition = null)
//    {
//        /**
//         * Если доп. условие не задано, пытаемся отдать существующие записи
//         */
//        if (empty($condition) && !empty($this->owner->isRelationPopulated($name))) {
//            return $this->owner->getRelation($name);
//        }
//
//        $res = null;
//        $quety = $this->getQuery($name);
//
//        if ($this->getRelType($quety) === self::REL_HAS_ONE) {
//            $res = $quety->one();
//        } else {
//            if (!empty($condition)) {
//                $quety->andWhere($condition);
//            }
//            $res = $quety->all();
//        }
//
//        /**
//         * Если доп. условие не задано, сохраняем в _relations модели
//         */
//        if (empty($condition)) {
//            $this->owner->populateRelation($name, $res);
//        }
//
//        return $res;
//    }
//
//    /**
//     * Возвращает тип связи
//     * @param \yii\db\ActiveQuery $query
//     * @return self::REL_HAS_ONE|self::REL_HAS_MANY|self::REL_MANY_MANY|false
//     */
//    public function getRelType($query)
//    {
//        $res = false;
//
//        if (!$query->multiple) {
//            $res = self::REL_HAS_ONE;
//        } else {
//            if ($query->via instanceof ActiveQuery || is_array($query->via)) {
//                $res = self::REL_MANY_MANY;
//            } elseif (empty($query->via)) {
//                $res = self::REL_HAS_MANY;
//            }
//        }
//
//        return $res;
//    }
//
//    /**
//     * Возвращает \yii\db\ActiveQuery для указанной связи
//
//     * @param string $name Имя связи
//     */
//    public function getQuery($name)
//    {
//        return $this->owner->getRelation($name);
//    }
//
//    /**
//     * Удаляет ссылки на связанные объекты из owner->_related
//     * @param string $name Имя связи
//     * @param int|int[]|ActiveRecord|ActiveRecord[] $items Удаляемые объекты
//     */
//    protected function _exludeFromRelated($name, $items)
//    {
//
//    }
//
//    /**
//     * Сохраняет в базу связь REL_HAS_ONE
//     * @param \yii\db\ActiveQuery $query \yii\db\ActiveQuery связуемого класса
//     * @param $data Данные
//     */
//    protected function _saveHasOne($query, $data)
//    {
//        if ($this->getRelType($query) !== self::REL_HAS_ONE) {
//            return false;
//        }
//
//
//    }
//
//    /**
//     * Сохраняет в базу связь REL_HAS_MANY
//     * @param \yii\db\ActiveQuery $query \yii\db\ActiveQuery связуемого класса
//     * @param $data Данные
//     */
//    protected function _saveHasMany($query, $data)
//    {
//        if ($this->getRelType($query) !== self::REL_HAS_MANY) {
//            return false;
//        }
//    }
//
//    /**
//     * Сохраняет в базу связь REL_MANY_MANY
//     * @param \yii\db\ActiveQuery $query \yii\db\ActiveQuery связуемого класса
//     * @param $data Данные
//     */
//    protected function _saveManyMany($query, $data)
//    {
//        if ($this->getRelType($query) !== self::REL_MANY_MANY) {
//            return false;
//        }
//    }
//}