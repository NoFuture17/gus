<?php
namespace app\components\behaviors;
use yii\base\Behavior;
use yii\base\ErrorException;
use yii\base\Object;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\db\ActiveRecordInterface;
/**
 * Saves Many-to-many relations
 */
class RelationalBehavior extends Behavior
{
    /**
     * @var ActiveRecord
     */
    public $owner;

    public function attach($owner)
    {
        if (!($owner instanceof ActiveRecord)) {
            throw new ErrorException('Owner must be instance of yii\db\ActiveRecord');
        }
        if (count($owner->getTableSchema()->primaryKey) > 1) {
            throw new ErrorException('RelationalBehavior doesn\'t support composite primary keys');
        }
        parent::attach($owner);
    }

    public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_INSERT => 'saveRelations',
            ActiveRecord::EVENT_AFTER_UPDATE => 'saveRelations',
            ActiveRecord::EVENT_BEFORE_UPDATE => 'saveRelations',
            ActiveRecord::EVENT_BEFORE_INSERT => 'saveRelations',
        ];
    }

    public function canSetProperty($name, $checkVars = true)
    {
        $getter = 'get' . $name;
        if (method_exists($this->owner, $getter) && $this->owner->$getter() instanceof ActiveQuery) {
            return true;
        }
        return parent::canSetProperty($name, $checkVars);
    }


    /**
     * Выборка связанных данных по ID
     *
     * Возможна множественная выборка
     *
     * @param string $name Название связи
     * @param int|int[] $id
     * @return ActiveRecord|ActiveRecord[]|null
     */
    public function getRelatedByPk($name, $id)
    {
        $getter = 'get' . $name;
        /** @var ActiveQuery $query */
        $query = $this->owner->$getter();
        /* @var $modelClass ActiveRecord */
        $modelClass = $query->modelClass;
        $value = null;
        if (is_array($id)) {
            $value = $modelClass::findAll($id);
        } elseif (is_numeric($id)) {
            $value = $modelClass::findOne($id);
        }
        return $value;
    }

    public function __set($name, $value)
    {
        if (empty($value)) {
            $this->deleteRelated($name);
        } elseif (!is_array($value)){
            $value = $this->getValidRelated($name, $value);
        } else {
            $value = array_map(function ($item) use($name) {
                return $this->getValidRelated($name, $item);
            }, $value);
        }

        $this->owner->populateRelation($name, $value);
    }

    /**
     * Удаляет связанные данные
     * @param sting $name Имя связи
     */
    public function deleteRelated($name)
    {
        $activeQuery = $this->owner->getRelation($name);
        $link = $activeQuery->link;
        $relatedModelColumn = array_keys($link)[0];
        $primaryModelColumn = $link[$relatedModelColumn];

        if (!$activeQuery->multiple) {
            $this->owner->$primaryModelColumn = null;
        } elseif (!empty($activeQuery->via)) { // для связи many-many

        } else {
            $items = $activeQuery->select(['id'])->all();
            foreach ($items as $item) {
                $item->$relatedModelColumn = null;
                $item->save();
            }
        }

        $this->owner->populateRelation($name, null);
    }

    /**
     * Сохраняет таблицу, связанную через hasOne
     *
     * @param ActiveQuery $query
     * @param array $relationRecords Массив со связанными данными
     */
    protected function _saveHasOne($query, $relationRecords)
    {
        $link = $query->link;
        $relatedModelColumn = array_keys($link)[0];
        $primaryModelColumn = $link[$relatedModelColumn];
        $data = (is_array($relationRecords)) ? current($relationRecords) : $relationRecords;
        $this->owner->$primaryModelColumn = $data->$relatedModelColumn;
    }

    /**
     * Сохраняет таблицу, связанную через hasMany
     *
     * @param ActiveQuery $query
     * @param array $relatedRecords Массив со связанными данными
     * @return bool
     */
    protected function _saveHasMany($query, $relatedRecords)
    {
        $link = $query->link;
        $relatedModelColumn = array_keys($link)[0];
        $primaryModelColumn = $link[$relatedModelColumn];
        if (!is_array($relatedRecords))
            return false;
        foreach ($relatedRecords as $row) {
            $row->$relatedModelColumn = $this->owner->$primaryModelColumn;
            $row->save();
        }
    }

    /**
     * Сохраняет таблицу, связанную через many-many
     *
     * @param ActiveQuery $activeQuery
     * @param array $relationRecords
     * @throws ErrorException
     */
    protected function _saveManyMany($activeQuery, $relationRecords)
    {
        /* @var $viaQuery ActiveQuery */
        if ($activeQuery->via instanceof ActiveQuery) {
            $viaQuery = $activeQuery->via;
        } elseif (is_array($activeQuery->via)) {
            $viaQuery = $activeQuery->via[1];
        } else {
            throw new ErrorException('Unknown via type');
        }
        $junctionTable = reset($viaQuery->from);
        $primaryModelColumn = array_keys($viaQuery->link)[0];
        $relatedModelColumn = reset($activeQuery->link);
        $junctionRows = [];
        $relationPks = ArrayHelper::getColumn($relationRecords, array_keys($activeQuery->link)[0], false);
        $passedRecords = count($relationPks);
        $relationPks = array_filter($relationPks);
        $savedRecords = count($relationPks);
        if ($passedRecords != $savedRecords) {
            throw new ErrorException('All relation records must be saved');
        }
        foreach ($relationPks as $relationPk) {
            $junctionRows[] = [$model->primaryKey, $relationPk];
        }
        $model->getDb()->transaction(function() use ($junctionTable, $primaryModelColumn, $relatedModelColumn,
            $junctionRows, $model
        ) {
            $db = $model->getDb();
            $db->createCommand()->delete($junctionTable, [$primaryModelColumn => $model->primaryKey])->execute();
            if (!empty($junctionRows)) {
                $db->createCommand()->batchInsert($junctionTable, [$primaryModelColumn, $relatedModelColumn],
                    $junctionRows)->execute();
            }
        });
    }

    /**
     * Функция сохранения связанных данных
     * @param \yii\base\Event $event
     * @throws ErrorException
     * @throws \Exception
     */
    public function saveRelations($event)
    {
        /** @var ActiveRecord $model */
        $model = $event->sender;
        $relatedRecords = $model->getRelatedRecords();
        foreach ($relatedRecords as $relationName => $relationRecords) {

            if (empty($relationRecords)) {
                continue;
            }

            $activeQuery = $model->getRelation($relationName);

            // Если не множественная связь
            if (!$activeQuery->multiple && in_array($event->name, [
                    ActiveRecord::EVENT_BEFORE_INSERT,
                    ActiveRecord::EVENT_BEFORE_UPDATE
                ])) {
                $this->_saveHasOne($activeQuery, $relationRecords);
            } elseif ($activeQuery->multiple && in_array($event->name, [
                    ActiveRecord::EVENT_AFTER_INSERT,
                    ActiveRecord::EVENT_AFTER_UPDATE
                ])) {
                // Если многие ко многим
                if (!empty($activeQuery->via)) {
                    $this->_saveManyMany();
                } else {
                    $this->_saveHasMany($activeQuery, $relationRecords);
                }
            }

            if (!empty($activeQuery->via)) { // works only for many-to-many relation

            }
        }
    }

    /**
     * Возвращает валидный объект из связанной таблицы $name по ID
     *
     * @param string $name
     * @param int|ActiveRecordInterface $value
     * @return ActiveRecordInterface|ActiveRecord
     * @throws ErrorException
     */
    public function getValidRelated($name, $value)
    {
        if (is_numeric($value)) {
            $value = $this->getRelatedByPk($name, $value);
        } elseif (!($value instanceof ActiveRecordInterface)) {
            throw new ErrorException('Данные не являются id или ActiveRecordInterface.');
        }
        return $value;
    }
}