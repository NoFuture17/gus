<?php

namespace app\modules\user\components\helpers;

class UserHelper {
    /**
     *
     * @var array Массив http адресов почтовых серверов и их доменов
     */
    public static $emailHosts   = [
        'mail.ru'           => ['bk.ru', 'mail.ru', 'inbox.ru', 'list.ru'],
        'mail.yandex.ru'    => [
            'yandex.ru', 'yandex.by', 'yandex.kz', 'yandex.com', 'yandex.ua', 
            'ya.ru', 'ya.by', 'ya.kz', 'ya.com',  'ya.ua', 
        ],
        'gmail.com'         => ['gmail.com'],
        'rambler.ru'        => ['autorambler.ru', 'lenta.ru', 'myrambler.ru', 'ro.ru']
    ];
    
    /**
     * Возвращает http адрес почтового сервера
     * @param string $emailAddress Адрес email
     */
    public static function getEmailHost($emailAddress)
    {
        if (empty($emailAddress) || !preg_match('/^\S+\@(\S+)$/', $emailAddress, $matches)) {
            return false;
        }

        $hostPart = $matches[1];
        $userEmailHost = null;

        foreach (self::$emailHosts as $host => $domains) {
            if (in_array($hostPart, $domains)){
                $userEmailHost = $host;
            }
        }

        return $userEmailHost;
    }
}