<?php
use yii\helpers\Html;
use app\modules\user\Module as UserModule;

/* @var $this yii\web\View */
/* @var $user app\modules\user\models\User */
$resetLink = Yii::$app->urlManager->createAbsoluteUrl([
    UserModule::$passwordResetUrlPath, 
    'token' => $user->password_reset_token
]);
?>
<div class="password-reset">
    <p>Hello <?= Html::encode($user->username) ?>,</p>

    <p>Follow the link below to reset your password:</p>

    <p><?= Html::a(Html::encode($resetLink), $resetLink) ?></p>
</div>