<?php
use app\modules\user\Module as UserModule;

/* @var $this yii\web\View */
/* @var $user app\modules\user\models\User */
$resetLink = Yii::$app->urlManager->createAbsoluteUrl([
    UserModule::$passwordResetUrlPath, 
    'token' => $user->password_reset_token
]);
?>
Hello <?= $user->username ?>,

Follow the link below to reset your password:

<?= $resetLink ?>