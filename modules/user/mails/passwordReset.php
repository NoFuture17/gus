<?php
use yii\helpers\Html;
use app\modules\user\Module as UserModule;
 
/* @var $this yii\web\View */
/* @var $user app\modules\user\models\User */
 
$resetLink = Yii::$app->urlManager->createAbsoluteUrl([
    UserModule::$passwordResetUrlPath, 'token' => $user->password_reset_token
]);
?>
 
Здравствуйте, <?= Html::encode($user->username) ?>!
 
Пройдите по ссылке, чтобы сменить пароль:
 
<?= Html::a(Html::encode($resetLink), $resetLink) ?>