<?php
/**
 * Created by PhpStorm.
 * User: nofuture17
 * Date: 03.12.15
 * Time: 14:03
 */

namespace app\modules\user\commands;

use yii\console\Controller;
use app\modules\user\models\User;
use Yii;


class RbacController extends Controller
{
    /**
     * Создаёт роли и привязывает к пользователям
     */
    public function actionInit()
    {
        $this->_rbacInit();
        $roles = $this->_createRbacData();
        $users = User::find()->asArray()->all();
        $this->_assignUsers($roles, $users);
    }

    public function actionMigrate()
    {
        $this->_rbacMigrate();
    }

    /**
     * Создаёт массив ролей
     *
     * @return array
     */
    protected function _createRbacData()
    {
        $result = [];
        $roles = User::getRolesArray();

        $role = Yii::$app->authManager->createRole(User::ROLE_USER);
        $role->description = $roles[User::ROLE_USER];
        Yii::$app->authManager->add($role);
        $result[User::ROLE_USER] = $role;

        $role = Yii::$app->authManager->createRole(User::ROLE_JOURNALIST);
        $role->description = $roles[User::ROLE_JOURNALIST];
        Yii::$app->authManager->add($role);
        Yii::$app->authManager->addChild($role, $result[User::ROLE_USER]);
        $result[User::ROLE_JOURNALIST] = $role;

        $role = Yii::$app->authManager->createRole(User::ROLE_MODERATOR);
        $role->description = $roles[User::ROLE_MODERATOR];
        Yii::$app->authManager->add($role);
        Yii::$app->authManager->addChild($role, $result[User::ROLE_JOURNALIST]);
        $result[User::ROLE_MODERATOR] = $role;

        $role = Yii::$app->authManager->createRole(User::ROLE_ADMIN);
        $role->description = $roles[User::ROLE_ADMIN];
        Yii::$app->authManager->add($role);
        Yii::$app->authManager->addChild($role, $result[User::ROLE_MODERATOR]);
        $result[User::ROLE_ADMIN] = $role;

        return $result;
    }

    /**
     * Привязывает роли к пользователям
     *
     * @param array $roles Существующие роли
     * @param array $users пользователи
     */
    protected function _assignUsers($roles, $users)
    {
        foreach ($users as $user) {
            Yii::$app->authManager->assign($roles[$user['role']], $user['id']);
        }
    }

    /**
     * Создаём миграцию и/или очищает старые данные
     */
    protected function _rbacMigrate() {
        $this->run('migrate/up', ['migrationPath' => '@yii/rbac/migrations/', 'interactive' => false]);
    }

    protected function _rbacInit()
    {
        Yii::$app->authManager->removeAll();
    }
}