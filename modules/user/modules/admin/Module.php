<?php

namespace app\modules\user\modules\admin;

class Module extends \app\components\modules\Admin
{
    public $controllerNamespace = 'app\modules\user\modules\admin\controllers';

    /**
     * @inheritdoc
     */
    public static function getMenuItems() {
        return [
            'section' => 'admin',
            'items' => [
                ['label' => 'Пользователи', 'url' => ['/user/admin/user/index']],
            ],
        ];
    }

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
