<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\bootstrap\Tabs;

/* @var $this yii\web\View */
/* @var $model app\modules\user\models\User */
/* @var $form yii\widgets\ActiveForm */

echo Tabs::widget([
    'items' => [
        [
            'label' => 'Пользователь',
            'content' => $this->render('_userForm', ['model' => $model]),
            'active' => true
        ],
        [
            'label' => 'Профиль',
            'content' => $this->render('_profileForm', ['model' => $model]),
        ],
    ],
]);
?>



