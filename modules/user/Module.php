<?php

namespace app\modules\user;

class Module extends \app\components\modules\Module
{
    public $controllerNamespace = 'app\modules\user\controllers';

    // Пути к важным действиям контроллеров
    public static $emailConfirmUrlPath = '/user/default/email-confirm';
    public static $passwordResetUrlPath = '/user/default/password-reset';
    public static $passwordResetRequestUrlPath = '/user/default/password-reset-request';

    public function init()
    {
        parent::init();

        $this->modules = [
            'admin' => [
                'class' => 'app\modules\user\modules\admin\Module',
            ]
        ];
    }

    public function getUrlRules()
    {
        return [
            [
                'rules' => [
                    '<action:(login|logout|signup|email-confirm|request-password-reset|password-reset)>' => 'user/default/<action>',
                ],
                'append' => false,
            ],
        ];
    }
}
