<?php
/**
 * Created by PhpStorm.
 * User: nofuture17
 * Date: 20.11.15
 * Time: 15:38
 */

namespace app\modules\user\models;


use app\components\models\SmartRecord;

class Profile extends SmartRecord
{
    public static function tableName()
    {
        return '{{%user_profile}}';
    }
}