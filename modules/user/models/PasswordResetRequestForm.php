<?php
namespace app\modules\user\models;
 
use app\modules\user\models\User;
use yii\base\Model;
/**
 * Password reset request form
 */
class PasswordResetRequestForm extends Model
{
    public $email;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'exist',
                'targetClass' => 'app\modules\user\models\User',
                'filter' => ['active' => User::ACTIVE_ACTIVE],
                'message' => 'Пользователь с данным email не зарегистрирован или заблокирован. Свяжитесь с администратором: ' .
                    \Yii::$app->params['SUPPORT_EMAIL']
            ],
        ];
    }
    /**
     * Sends an email with a link, for resetting the password.
     *
     * @return boolean whether the email was send
     */
    public function sendEmail()
    {
        /* @var $user User */
        $user = User::findOne([
            'active' => User::ACTIVE_ACTIVE,
            'email' => $this->email,
        ]);
        if ($user) {
            if (!User::isPasswordResetTokenValid($user->password_reset_token)) {
                $user->generatePasswordResetToken();
            }
            if ($user->save()) {
                return \Yii::$app->mailer->compose(
                        [
                            'html' => '@app/modules/user/mails/passwordResetToken-html', 
                            'text' => '@app/modules/user/mails/passwordResetToken-text'
                        ], 
                        ['user' => $user]
                    )
                    ->setFrom([\Yii::$app->params['SUPPORT_EMAIL'] => \Yii::$app->name . ' робот'])
                    ->setTo($this->email)
                    ->setSubject('Сброс пароля для: ' . \Yii::$app->name . ' - ' . \Yii::$app->request->hostInfo)
                    ->send();
            }
        }
        return false;
    }
}