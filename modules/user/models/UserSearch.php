<?php

namespace app\modules\user\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\user\models\User;

/**
 * UserSearch represents the model behind the search form about `app\modules\user\models\User`.
 */
class UserSearch extends Model
{
    
    public $id;
    public $username;
    public $email;
    public $active;
    public $role;
    public $created_from;
    public $created_to;
    public $updated_from;
    public $updated_to;
    
    public function rules()
    {
        return [
            [['id', 'role', 'active'], 'integer'],
            [['username', 'email'], 'safe'],
            [['created_from', 'created_to', 'updated_from', 'updated_to'], 'date', 'format' => 'php:Y-m-d'],
        ];
    }
 
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_at'    => 'Создан',
            'updated_at'    => 'Обновлён',
            'username'      => 'Имя',
            'email'         => 'Email',
            'active'        => 'Активность',
            'role'          => 'Привилегии',
            'created_from'  => 'Создан от',
            'created_to'    => 'Создан до',
            'updated_from'  => 'Обновлён от',
            'updated_to'    => 'Обновлён до',
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = User::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['id' => SORT_DESC],
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'role' => $this->role,
            'active' => $this->active,
        ]);

        $query->andFilterWhere(['like', 'username', $this->username])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['>=', 'updated_at', $this->updated_from ? strtotime($this->updated_from . ' 00:00:00') : null])
            ->andFilterWhere(['<=', 'updated_at', $this->updated_to ? strtotime($this->updated_to . ' 23:59:59') : null])
            ->andFilterWhere(['>=', 'created_at', $this->created_from ? strtotime($this->created_from . ' 00:00:00') : null])
            ->andFilterWhere(['<=', 'created_at', $this->created_to ? strtotime($this->created_to . ' 23:59:59') : null]);

        return $dataProvider;
    }
}