<?php

namespace app\modules\test\models;

/**
 * This is the ActiveQuery class for [[TestOne]].
 *
 * @see TestOne
 */
class TestOneQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return TestOne[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return TestOne|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}