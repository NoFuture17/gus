<?php

namespace app\modules\test\models;

use Yii;

/**
 * This is the model class for table "test_one".
 *
 * @property integer $id
 */
class TestOne extends \yii\db\ActiveRecord
{
    public function behaviors()
    {
        return [
            'relational' => [
                'class' => '\app\components\behaviors\RelationalBehavior',
            ],
        ];
    }

    public function getTestTwo()
    {
        return $this->hasMany('\app\modules\test\models\TestTwo', ['id_parent' => 'id']);
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'test_one';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
        ];
    }

    /**
     * @inheritdoc
     * @return TestOneQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TestOneQuery(get_called_class());
    }
}
