<?php

namespace app\modules\test\models;

use Yii;

/**
 * This is the model class for table "test_two".
 *
 * @property integer $id
 * @property integer $id_parent
 */
class TestTwo extends \yii\db\ActiveRecord
{
    public function behaviors()
    {
        return [
            'relational' => [
                'class' => '\app\components\behaviors\RelationalBehavior',
            ],
        ];
    }

    public function getTestOne()
    {
        return $this->hasOne('\app\modules\test\models\TestOne', ['id' => 'id_parent']);
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'test_two';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_parent'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_parent' => 'Id Parent',
        ];
    }

    /**
     * @inheritdoc
     * @return TestTwoQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TestTwoQuery(get_called_class());
    }
}
