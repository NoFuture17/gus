<?php

namespace app\modules\test\models;

use Yii;

/**
 * This is the model class for table "test_three".
 *
 * @property integer $id
 */
class TestThree extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'test_three';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
        ];
    }

    /**
     * @inheritdoc
     * @return TestThreeQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TestThreeQuery(get_called_class());
    }
}
