<?php

namespace app\modules\test\models;

/**
 * This is the ActiveQuery class for [[TestTwo]].
 *
 * @see TestTwo
 */
class TestTwoQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return TestTwo[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return TestTwo|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}