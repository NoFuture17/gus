<?php

namespace app\modules\test\models;

/**
 * This is the ActiveQuery class for [[TestThree]].
 *
 * @see TestThree
 */
class TestThreeQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return TestThree[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return TestThree|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}