<?php

namespace app\modules\test\controllers;

use yii\web\Controller;
use \app\modules\test\models\TestOne;
use \app\modules\test\models\TestTwo;
use \yii\helpers;

class DefaultController extends Controller
{
    public function actionIndex()
    {
        $testOne = TestOne::find()->one();
        $testTwo = TestTwo::find()->all();
        $testOne->testTwo = $testTwo;
        $testOne->save();
        $testOne->testTwo = null;
    }

    public function actionTestTwo()
    {

        $testOne = TestOne::find()->one();
        $testTwo = TestTwo::find()->one();

        $testTwo->testOne = null;
        $testTwo->save();

        $testTwo->testOne = $testOne;
        $testTwo->save();
    }
}
