<?php

namespace app\modules\news;

class Module extends \app\components\modules\Module
{
    public $controllerNamespace = 'app\modules\news\controllers';

    public function init()
    {
        parent::init();
        $this->modules = [
            'admin' => [
                'class' => '\app\modules\news\modules\admin\Module',
            ],
        ];
    }
}
