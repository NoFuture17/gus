<?php

namespace app\modules\news\controllers;

use app\components\helpers\MenuCreateHelper;
use yii\web\Controller;
use \app\modules\news\models\NewsCategory;
use \app\modules\news\models\News;

class DefaultController extends \app\components\controllers\PublicController
{
    public function actionIndex()
    {
        $news = News::find()->all();
        return $this->render('index',
            [
                'news' => $news,
            ]
        );
    }

    public function actionCategory($url)
    {
        $category = NewsCategory::find()->newsCategory($url)->one();
        $result = $category->getMenuItems();
        echo \yii\bootstrap\Nav::widget(['items' => $result]);
        $breadcrumbs = $category->getBreadcrumbsItems();
        echo \yii\widgets\Breadcrumbs::widget(['links' => $breadcrumbs]);
        $news = $category->getNews()->all();
        return $this->render('index',
            [
                'news' => $news,
            ]
        );
    }
}
