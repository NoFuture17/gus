<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\news\models\News */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="news-form">

    <?php $form = ActiveForm::begin([
        'options' => ['enctype'=>'multipart/form-data']
    ]); ?>

    <?= \app\components\widgets\form\AdminPageFieldSet::widget([
        'model' => $model,
        'form' => $form
    ]) ?>

    <?php
    echo \app\components\widgets\form\SmartInput::widget([
        'model'     => $model,
        'attribute' => 'id_category',
        'label'     => true,
        'form'      => $form,
    ]);
    ?>

    <?php
    echo \app\components\widgets\form\SmartInput::widget([
        'model'     => $model,
        'attribute' => 'ImageGallery',
        'label'     => false,
        'form'      => $form,
        'options'   => [
            'action' => ['/news/admin/news/image-gallery']
        ],
    ]);
    ?>

    <?php
    echo \app\components\widgets\form\SmartInput::widget([
        'model'     => $model,
        'attribute' => 'VideoGallery',
        'label'     => false,
        'form'      => $form,
        'options'   => [
            'action' => ['/news/admin/news/video-gallery']
        ],
    ]);
    ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
