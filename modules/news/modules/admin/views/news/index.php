<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\news\models\NewsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>
<div class="news-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить новость', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'id',
            'created_at:date',
            [
                'class' => 'app\components\widgets\grid\SetColumn',
                'attribute' => 'priority',
                'setFilter' => true,
                'formAction' => [
                    'route' => '/news/admin/news/update',
                    'params' => ['id' => ':id']
                ],
            ],
            [
                'class' => 'app\components\widgets\grid\SetColumn',
                'attribute' => 'active',
                'setFilter' => true,
                'formAction' => [
                    'route' => '/news/admin/news/update',
                    'params' => ['id' => ':id']
                ],
            ],
            [
                'class' => 'app\components\widgets\grid\SetColumn',
                'attribute' => 'id_category',
                'setFilter' => true,
                'formAction' => [
                    'route' => '/news/admin/news/update',
                    'params' => ['id' => ':id']
                ],
            ],
            // 'author',
            [
                'class' => 'app\components\widgets\grid\SetColumn',
                'attribute' => 'name',
                'setFilter' => true,
                'formAction' => [
                    'route' => '/news/admin/news/update',
                    'params' => ['id' => ':id']
                ],
            ],
            [
                'class' => 'app\components\widgets\grid\SetColumn',
                'attribute' => 'url',
                'setFilter' => true,
                'formAction' => [
                    'route' => '/news/admin/news/update',
                    'params' => ['id' => ':id']
                ],
            ],
            // 'name',
            // 'url:url',
            // 'meta_title',
            // 'meta_keywords',
            // 'meta_description',
            // 'text_small:ntext',
            // 'text_full:ntext',
            // 'image_small',
            // 'image_full',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update}{delete}',
            ],
        ],
    ]); ?>

</div>
