<?php

namespace app\modules\news\modules\admin;

class Module extends \app\components\modules\Admin
{
    public $controllerNamespace = 'app\modules\news\modules\admin\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }

    public static function getMenuItems() {
        return [
            'section' => 'admin',
            'items' => [
                [
                    'label' => 'Новости',
                    'items' => [
                        ['label' => 'Новости', 'url' => ['/news/admin/news/index']],
                        ['label' => 'Категории', 'url' => ['/news/admin/news-category/index']],
                    ]
                ]
            ],
        ];
    }
}
