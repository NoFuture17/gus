<?php

namespace app\modules\news\models;

/**
 * This is the ActiveQuery class for [[NewsCategory]].
 *
 * @see NewsCategory
 */
class NewsCategoryQuery extends \app\components\models\PageQuery
{

    public function behaviors()
    {
        return \yii\helpers\ArrayHelper::merge(
            parent::behaviors(),
            [
                'nestedCategoryQuery' => [
                    'class' => '\app\components\behaviors\nestedsets\NestedSetsQueryBehavior',
                ],
            ]
        );
    }

    /**
     * Настраивает запрос для поля ввода
     * @return $this
     */
    public function toInput()
    {
       return $this->active();
    }

    /**
     * Набор условий для активной к-ии с $url
     * @param string $url
     * @return $this
     */
    public function newsCategory($url)
    {
        return $this->active()->url($url);
    }

    /**
     * Набор условий для выбоки в меню
     * @return $this
     */
    public function toMenu()
    {
        return $this->active()->orderBy('priority DESC, depth ASC');
    }

    /**
     * Набор условий для выбоки в хлебные крошки
     * @return $this
     */
    public function toBreadcrumbs()
    {
        return $this->active()->addOrderBy('depth ASC');
    }

    /**
     * @inheritdoc
     * @return NewsCategory[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return NewsCategory|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}