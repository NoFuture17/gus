<?php

namespace app\modules\news\models;

/**
 * This is the ActiveQuery class for [[News]].
 *
 * @see News
 */
class NewsQuery extends \app\components\models\PageQuery
{
    /**
     * Настраивает условие для поиска новостей в категории
     * @param int $category
     * @return $this
     */
    public function toList()
    {
        return $this->active()->orderBy('created_at DESC, priority DESC');
    }

    /**
     * @inheritdoc
     * @return News[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return News|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}