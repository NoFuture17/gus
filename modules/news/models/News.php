<?php

namespace app\modules\news\models;

use Yii;
use \yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%news}}".
 *
 * @property integer $id_category
 */
class News extends \app\components\models\Page
{
    const IMAGES_URL_ALIAS = '@web/data/images/news/';

    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                'imageGallery' => [
                    'class' => '\app\components\behaviors\GalleryManager',
                    'relationModelName' => '\app\modules\news\models\NewsImageGalleryItem',
                    'galleryAttribute' => 'ImageGallery'
                 ],
                'videoGallery' => [
                    'class' => '\app\components\behaviors\GalleryManager',
                    'relationModelName' => '\app\modules\news\models\NewsVideoGalleryItem',
                    'galleryAttribute' => 'VideoGallery',
                ],
            ]
        );
    }

    public function transactions()
    {
        return ['default' => self::OP_ALL];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%news}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return ArrayHelper::merge(
            parent::rules(), [
                [['id_category'], 'integer'],
            ]
        );
    }

    // Методы SmartInputInterface
    public function getInputType($attribute)
    {
        $result = null;

        switch ($attribute) {
            case 'id_category':
                $result = \app\components\widgets\form\SmartInput::TYPE_SELECT;
                break;
            case 'ImageGallery':
                $result = \app\components\widgets\form\SmartInput::TYPE_GALLERY_IMAGE;
                break;
            case 'VideoGallery':
                $result = \app\components\widgets\form\SmartInput::TYPE_GALLERY_VIDEO;
                break;
            default:
                $result = parent::getInputType($attribute);
        }

        return $result;
    }

    public function getInputData($attribute)
    {
        $result = null;

        switch ($attribute) {
            case 'id_category':
                $result = \yii\helpers\ArrayHelper::merge(
                    ['0' => 'Без родителя'],
                    ArrayHelper::map($this->getCategory()->clean()->toInput()->all(), 'id', 'name')
                );
                break;
            case 'ImageGallery':
                $result = $this->getBehavior('imageGallery');
                break;
            case 'VideoGallery':
                $result = $this->getBehavior('videoGallery');
                break;
            default:
                $result = parent::getInputData($attribute);
        }

        return $result;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'id_category' => 'Категория',
            'ImageGallery' => 'Галерея картинок',
        ]);
    }

    /**
     * @return NewsCategoryQuery
     */
    public function getCategory()
    {
        return $this->hasOne('\app\modules\news\models\NewsCategory', ['id' => 'id_category']);
    }

    /**
     * @inheritdoc
     * @return NewsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new NewsQuery(get_called_class());
    }
}
