<?php

namespace app\modules\news\models;

use Yii;
use \yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%news_category}}".
 *
 * @property integer $lft
 * @property integer $rgt
 * @property integer $depth
 * @property integer $id
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $priority
 * @property integer $active
 * @property integer $author
 * @property string $name
 * @property string $url
 * @property string $meta_title
 * @property string $meta_keywords
 * @property string $meta_description
 * @property string $text_small
 * @property string $text_full
 * @property string $image_small
 * @property string $image_full
 */
class NewsCategory extends \app\components\models\Page
{
    const IMAGES_URL_ALIAS = '@web/data/images/news/category/';

    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                'nestedCategory' => [
                    'class' => '\app\components\behaviors\NestedCategoryBehavior',
                    'treeAttribute' => 'tree',
                ],
            ]
        );
    }

    public function transactions()
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_ALL,
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%news_category}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return ArrayHelper::merge(
            parent::rules(),
            [
                ['id_parent', 'integer'],
                ['id_parent', 'default', 'value' => 0],
            ]
        );
    }

    // Методы SmartInputInterface
    public function getInputType($attribute)
    {
        $result = null;

        switch ($attribute) {
            case 'id_parent':
                $result = \app\components\widgets\form\SmartInput::TYPE_SELECT;
                break;
            default:
                $result = parent::getInputType($attribute);
        }

        return $result;
    }

    public function getInputData($attribute)
    {
        $result = null;

        switch ($attribute) {
            case 'id_parent':
                $result = $this->getBehavior('nestedCategory')->getParentInputData();
                break;
            default:
                $result = parent::getInputData($attribute);
        }

        return $result;
    }

    public function getItemUrl() {
        if ($this->isNewRecord)
            return false;

        return \yii\helpers\Url::to(['/news/default/category', 'url' => $this->url]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'id_parent' => 'Родительская',
        ]);
    }

    /**
     * @return NewsCategoryQuery
     */
    public function getParent()
    {
        return $this->hasOne($this->className(), ['id' => 'id_parent']);
    }

    /**
     * Возвращает Query новостей данной категории
     */
    public function getCategoryItems()
    {
        return $this->hasMany('\app\modules\news\models\News', ['id_category' => 'id'])->toList();
    }

    /**
     * @inheritdoc
     * @return NewsCategoryQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new NewsCategoryQuery(get_called_class());
    }
}
