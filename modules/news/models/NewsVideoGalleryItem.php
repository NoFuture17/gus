<?php
/**
 * User: lagprophet
 * Date: 29.12.15
 * Time: 14:06
 */

namespace app\modules\news\models;


class NewsVideoGalleryItem extends \app\components\models\gallery\VideoGalleryItem
{
    public static function tableName()
    {
        return '{{%news_video_gallery}}';
    }

    /**
     * @inheritdoc
     * @return NewsVideoGalleryItemQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new NewsVideoGalleryItemQuery(get_called_class());
    }

}