<?php
/**
 * User: lagprophet
 * Date: 22.12.15
 * Time: 15:11
 */

namespace app\modules\news\models;


class NewsImageGalleryItem extends \app\components\models\gallery\ImageGalleryItem
{
    public static function tableName()
    {
        return '{{%news_image_gallery}}';
    }

    /**
     * @inheritdoc
     * @return NewsImageGalleryItemQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new NewsImageGalleryItemQuery(get_called_class());
    }

}