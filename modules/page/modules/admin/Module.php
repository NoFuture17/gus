<?php

namespace app\modules\page\modules\admin;

class Module extends \app\components\modules\Admin
{
    public $controllerNamespace = 'app\modules\page\modules\admin\controllers';

    public static function getMenuItems() {
        return [
            'section' => 'admin',
            'items' => [
                ['label' => 'Статические страницы', 'url' => ['/page/admin/default/index']],
            ],
        ];
    }

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
