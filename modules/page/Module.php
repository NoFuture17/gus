<?php

namespace app\modules\page;

class Module extends \app\components\modules\Module implements \yii\base\BootstrapInterface
{
    public $controllerNamespace = 'app\modules\page\controllers';

    public function init()
    {
        parent::init();
        $this->modules = [
            'admin' => [
                'class' => '\app\modules\page\modules\admin\Module',
            ],
        ];
    }

    public function getUrlRules()
    {
        return [
            [
                'rules' => [new \app\modules\page\urlRules\StaticPage],
                'append' => false,
            ],
        ];
    }
}
