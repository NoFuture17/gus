<?php
/**
 * Created by PhpStorm.
 * User: nofuture17
 * Date: 30.11.15
 * Time: 16:14
 */

namespace app\modules\page\urlRules;

use yii\web\UrlRuleInterface;
use yii\base\Object;
use app\modules\page\models\Page;

class StaticPage extends Object implements UrlRuleInterface
{

    public function createUrl($manager, $route, $params)
    {
        if ($route === 'page/default/index' && isset($params['url'])) {
            if ($params['url'] === 'main')
                return $manager->baseUrl;
            else
                return $manager->baseUrl . $params['url'];
        }

        return false;  // this rule does not apply
    }

    public function parseRequest($manager, $request)
    {
        $pathInfo = $request->getPathInfo();

        if ($pathInfo === '') {
            $model = Page::find()->active()->url('main')->one();

            if (!empty($model))
                return ['page/default/index', ['url' => $model->url]];
        }

        if (preg_match('%^(\w+)$%', $pathInfo, $matches)) {
            $model = Page::find()->active()->url($matches[1])->one();

            if (!empty($model))
                return ['page/default/index', ['url' => $model->url]];
        }

        return false;  // this rule does not apply
    }
}