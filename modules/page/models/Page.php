<?php

namespace app\modules\page\models;

use Yii;
use \app\components\behaviors\ImageUploadBehavior;
use \yii\helpers\ArrayHelper;

/**
 * This is the model class for table "page".
 *
 * @property integer $id
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $priority
 * @property integer $active
 * @property integer $author
 * @property string $name
 * @property string $url
 * @property string $meta_title
 * @property string $meta_keywords
 * @property string $meta_description
 * @property string $text_small
 * @property string $text_full
 * @property string $image_small
 * @property string $image_full
 */
class Page extends \app\components\models\Page
{

    const IMAGES_PATH_ALIAS = '@webroot/data/images/page/';
    const IMAGES_URL_ALIAS = '@web/data/images/page/';

    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), []);
    }

    public function getItemUrl() {
        if ($this->isNewRecord)
            return false;

        return Url::to(['/page/default/index', 'url' => $this->url]);
    }


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%page}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), []);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), []);
    }

    /**
     * @inheritdoc
     * @return PageQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PageQuery(get_called_class());
    }
}
