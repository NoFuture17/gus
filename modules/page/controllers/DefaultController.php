<?php

namespace app\modules\page\controllers;

use yii\web\Controller;

class DefaultController extends \app\components\controllers\PublicController
{

    public function actionIndex($url)
    {
        // Получаем страницу
        $pageData = \app\modules\page\models\Page::find()->active()->url($url)->one();

        // Задаём мета теги
        $this->view->title = $pageData->meta_title;
        $this->view->registerMetaTag(['name' => 'keywords', 'content' => $pageData->meta_keywords]);
        $this->view->registerMetaTag(['name' => 'description', 'content' => $pageData->meta_description]);

        return $this->render('index', ['pageData' => $pageData]);
    }
}
