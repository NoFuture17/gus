<?php

namespace app\modules\gus\models;

use Yii;

/**
 * This is the model class for table "{{%yandex_account}}".
 *
 * @property integer $id
 * @property integer $active
 * @property string $name
 * @property string $password
 * @property string $translate_token
 */
class YandexAccount extends \app\components\models\SmartRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%yandex_account}}';
    }

    // Методы SmartInputInterface
    public function getInputType($attribute)
    {
        $result = null;

        switch ($attribute) {
            default:
                $result = parent::getInputType($attribute);
        }

        return $result;
    }

    public function getInputData($attribute)
    {
        $result = null;

        switch ($attribute) {
            default:
                $result = parent::getInputData($attribute);
        }

        return $result;
    }

    public function getInputOptions($attribute)
    {
        return parent::getInputOptions($attribute);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['active', 'id'], 'integer'],
            [['name', 'password', 'translate_token'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'active' => 'Active',
            'name' => 'Name',
            'password' => 'Password',
            'translate_token' => 'Translate Token',
        ];
    }

    /**
     * @inheritdoc
     * @return YandexAccountQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new YandexAccountQuery(get_called_class());
    }
}
