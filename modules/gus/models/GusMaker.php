<?php
/**
 * Created by PhpStorm.
 * User: nofuture17
 * Date: 26.03.16
 * Time: 14:35
 */

namespace app\modules\gus\models;


use app\components\models\Page;
use yii\base\Component;

class GusMaker extends Component
{
    /**
     * Модель к которой будут лепиться новые елементы
     * @var \app\components\models\SmartRecord
     */
    public $parentNode;

    /**
     * Модель текущего нового элемента
     * @var \app\components\models\SmartRecord
     */
    public $currentNode;

    /**
     * Максимальный размер соты
     * @var int
     */
    public $maxSoteSize;

    /**
     * Минимальный размер соты
     * @var int
     */
    public $minSoteSize;

    /**
     * Размер текущей родительской соты
     * @var int
     */
    public $soteSize;

    /**
     * Пустой объект модели соты
     * @var \app\components\models\SmartRecord
     */
    public $soteModel;

    public function make($items) {
        $sotesCount = $this->soteModel->find()->count();
        if ($sotesCount < 1) {
            $this->createFirstSote();
        }

        for ($k = 0; $cnt1 = count($items), $k < $cnt1; $k++) {
            $this->parentNode = $this->getNotFullSote();

            $this->soteSize = $this->generateSoteSize();
            $childCount = $this->parentNode->getBehavior('nestedSotes')->children(1)->count();

            for ($i = 1; $cnt = count($items), $i <= $cnt; $i++) {
                if ($childCount >= $this->soteSize) {
                    break;
                }

                $itemData = array_shift($items);
                $newSote = $this->generateNewCote($itemData);

                if (!$newSote) {
                    continue;
                }

                if (!$this->addNewSoteToParent($newSote)) {
                    continue;
                }
                $childCount++;
            }
        }
    }

    /**
     * Найти неполностью заполненную соту
     * @return \app\components\models\SmartRecord
     */
    public function getNotFullSote() {
        $diff = (((int) $this->maxSoteSize * 2) + 1);

        return $sote = $this->soteModel->find()->noCache()
            ->select('*, `rgt` - `lft` as `diff`')
            ->where('1')
            ->having('`diff` < ' . $diff)
            ->orderBy('`diff` DESC, id ASC')->one();
    }

    /**
     * Создать модель соты для дальнейшего заполнения
     * @return \app\components\models\SmartRecord
     */
    public function createNewSoteModel() {
        $soteClassName = get_class($this->soteModel);
        return new $soteClassName ();
    }

    public function createFirstSote() {
        $firstSote = $this->createNewSoteModel();
        $firstSote->name = "Первая сота";
        $firstSote->active = Page::ACTIVE_ACTIVE;
        $firstSote->url = "first-sote";
        $firstSote->save();
    }

    /**
     * Добавить новую соту к родительской
     * @param \app\components\models\SmartRecord $newSote
     */
    public function addNewSoteToParent(\app\components\models\SmartRecord $newSote) {
        return $newSote->getBehavior('nestedSotes')->appendTo($this->parentNode);
    }

    public function generateNewCote($itemData) {
        $sote = $this->createNewSoteModel();

        foreach ($itemData as $attribute => $value) {
            $sote->setAttribute($attribute, $value);
        }
        $sote->url = time() . rand(1, 1000);
        $sote->active = Page::ACTIVE_ACTIVE;
        return $sote;
    }

    /**
     * Создать случайный размер для текущей родительской соты
     * @return int
     */
    public function generateSoteSize() {
        $this->soteSize = rand($this->minSoteSize, $this->maxSoteSize);
        return $this->soteSize;
    }
}