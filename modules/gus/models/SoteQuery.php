<?php

namespace app\modules\gus\models;

/**
 * This is the ActiveQuery class for [[Sote]].
 *
 * @see Sote
 */
class SoteQuery extends \app\components\models\PageQuery
{
    public function behaviors()
    {
        return \yii\helpers\ArrayHelper::merge(
            parent::behaviors(),
            [
                'nestedCategoryQuery' => [
                    'class' => '\app\components\behaviors\nestedsets\NestedSetsQueryBehavior',
                ],
            ]
        );
    }

    /**
     * @inheritdoc
     * @return Sote[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Sote|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}