<?php

namespace app\modules\gus\models;

use Yii;
use yii\base\Exception;

/**
 * This is the model class for table "sote".
 *
 * @property integer $id
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $priority
 * @property integer $active
 * @property integer $author
 * @property string $name
 * @property string $name_small
 * @property string $url
 * @property string $meta_title
 * @property string $meta_keywords
 * @property string $meta_description
 * @property string $text_small
 * @property string $text_full
 * @property string $links
 * @property string $snipets
 * @property string $image_small
 * @property string $image_full
 */
class Sote extends \app\components\models\Page
{
    const IMAGES_PATH_ALIAS = '@webroot/data/images/sote/';
    const IMAGES_URL_ALIAS = '@web/data/images/sote/';

    public function behaviors()
    {
        return \yii\helpers\ArrayHelper::merge(parent::behaviors(),
            [
                'nestedSotes' => [
                    'class' => '\app\components\behaviors\NestedCategoryBehavior',
//                    'treeAttribute' => 'tree',
                ],
            ]
        );
    }

    public function beforeValidate()
    {
        return parent::beforeValidate();
        $this->url = rand(0, 1000);
    }

    public function getItemUrl() {
        if ($this->isNewRecord)
            return false;

        $host = (
            !empty(\Yii::$app->params['HOST']) &&
            \Yii::$app->params['HOST'] != 'example.com'
        ) ? \Yii::$app->params['HOST'] : '';

        $parentItems = $this->getBehavior('nestedSotes')
            ->parents()
            ->toBreadcrumbs()
            ->active()
            ->noRoot()
            ->all();

        if (\Yii::$app->params['GUS.REVERS'] == '0') {
            $url = $host;

            if (!empty($parentItems)) {
                foreach ($parentItems as $parent) {
                    $url .= '/' . $parent->id;
                }
            }

            $url .= '/' . $this->id;

            return \yii\helpers\Url::to(['/gus/default/index', 'url' => $url]);
        } else {
            if (empty($host)) {
                throw new Exception('Необходимо указать параметр "HOST"');
            }
        }
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%sote}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return \yii\helpers\ArrayHelper::merge(parent::rules(), []);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return \yii\helpers\ArrayHelper::merge(parent::attributeLabels(), []);
    }

    /**
     * @inheritdoc
     * @return SoteQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new SoteQuery(get_called_class());
    }
}
