<?php

namespace app\modules\gus\models;

/**
 * This is the ActiveQuery class for [[YandexAccount]].
 *
 * @see YandexAccount
 */
class YandexAccountQuery extends \app\components\models\SmartQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return YandexAccount[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return YandexAccount|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}