<?php

namespace app\modules\gus\models;

use Yii;

/**
 * This is the model class for table "{{%parser_task}}".
 *
 * @property integer $id
 * @property integer $active
 * @property string $name
 * @property integer $result_count
 * @property string $data
 */
class ParserTask extends \app\components\models\SmartRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%parser_task}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['active', 'result_count'], 'integer'],
            [['data'], 'string'],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'active' => 'Active',
            'name' => 'Name',
            'result_count' => 'Result Count',
            'data' => 'Data',
        ];
    }

    /**
     * @inheritdoc
     * @return ParserTaskQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ParserTaskQuery(get_called_class());
    }
}
