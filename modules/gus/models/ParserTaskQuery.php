<?php

namespace app\modules\gus\models;

/**
 * This is the ActiveQuery class for [[ParserTask]].
 *
 * @see ParserTask
 */
class ParserTaskQuery extends \app\components\models\SmartQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return ParserTask[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return ParserTask|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}