<?php
/**
 * Created by PhpStorm.
 * User: nofuture17
 * Date: 03.04.16
 * Time: 7:00
 */

namespace app\modules\gus\models;


use yii\base\Model;

class CreateParsterTaskForm extends Model
{
    public $startUrl;
    public $limit;
    public $filterUrlRules;
    public $filterUrlToParseRules;
    public $name;
    public $name_small;
    public $url;
    public $meta_title;
    public $meta_keywords;
    public $meta_description;
    public $text_small;
    public $text_full;
    public $translate_language;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['startUrl', 'name'], 'required'],
            [['limit'], 'integer'],
            [
                [
                    'startUrl',
                    'filterUrlRules',
                    'filterUrlToParseRules',
                    'translate_language',
                    'name',
                    'name_small',
                    'url',
                    'meta_title',
                    'meta_keywords',
                    'meta_description',
                    'text_small',
                    'text_full',
                ],
                'string', 'max' => 255
            ]
        ];
    }

    public function attributeLabels()
    {
        return [
            'startUrl' => 'Сайт http://example.com',
            'limit' => 'Лимит (200)',
            'filterUrlRules' => 'Правила фильтрации урлов(regexp на новой строке)',
            'filterUrlToParseRules' => 'Правила фильтрации урлов откуда парсить',
            'translate_language' => 'Направление перевода',
            'name' => 'Название',
            'name_small' => 'Краткое название',
            'url' => 'Урл (не будет работать:D)',
            'meta_title' => 'title',
            'meta_keywords' => 'Ключевые слова',
            'meta_description' => 'Мета-тег описание',
            'text_small' => 'Краткий текст',
            'text_full' => 'Полный текст',
        ];
    }

    public function getSelectors() {
        return [
            'name' => $this->name,
            'name_small' => $this->name_small,
            'url' => $this->url,
            'meta_title' => $this->meta_title,
            'meta_keywords' => $this->meta_keywords,
            'meta_description' => $this->meta_description,
            'text_small' => $this->text_small,
            'text_full' => $this->text_full,
        ];
    }

    public function getFilterUrlRules($attribute) {
        $rules = explode(PHP_EOL, $this->{$attribute});
        foreach ($rules as $key => $value) {
            $rules[$key] = '/' . $value . '/';
        }
        return $rules;
    }
}