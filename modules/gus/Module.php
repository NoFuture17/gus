<?php

namespace app\modules\gus;

class Module extends \app\components\modules\Module implements \yii\base\BootstrapInterface
{
    public $controllerNamespace = 'app\modules\gus\controllers';

    public function init()
    {
        parent::init();
        $this->modules = [
            'admin' => [
                'class' => '\app\modules\gus\modules\admin\Module',
            ],
        ];
    }

    public function getUrlRules()
    {
        return [
            [
                'rules' => [new \app\modules\gus\urlRules\Sote],
                'append' => false,
            ],
        ];
    }
}
