<?php

namespace app\modules\gus\modules\admin;

class Module extends \app\components\modules\Admin
{
    public $controllerNamespace = 'app\modules\gus\modules\admin\controllers';

    public static function getMenuItems() {
        return [
            'section' => 'admin',
            'items' => [
                [
                    'label' => 'Гусь',
                    'items' => [
                        ['label' => 'Соты', 'url' => ['/gus/admin/sote/index']],
                        ['label' => 'Аккаунты яндекса', 'url' => ['/gus/admin/yandex-account/index']],
                        ['label' => 'Парсер', 'url' => ['/gus/admin/parser/index']],
                    ],
                ]
            ],
        ];
    }
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
