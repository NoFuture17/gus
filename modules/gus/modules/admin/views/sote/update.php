<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\gus\models\Sote */

$this->title = 'Update Sote: ' . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Sotes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="sote-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
