<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\gus\models\ParserTask */

?>
<div class="parser-task-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_create_form', [
        'model' => $model,
        'supportedLanguages' => $supportedLanguages,
    ]) ?>

</div>
