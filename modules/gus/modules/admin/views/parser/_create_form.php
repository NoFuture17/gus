<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\gus\models\ParserTask */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="parser-task-create-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'startUrl')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'limit')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'filterUrlRules')->textarea(['rows' => 6]) ?>
    <?= $form->field($model, 'filterUrlToParseRules')->textarea(['rows' => 6]) ?>
    <?= $form->field($model, 'translate_language')->dropDownList($supportedLanguages) ?>
    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'name_small')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'url')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'meta_title')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'meta_keywords')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'meta_description')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'text_small')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'text_full')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Create', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
