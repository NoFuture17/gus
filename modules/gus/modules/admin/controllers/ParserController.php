<?php

namespace app\modules\gus\modules\admin\controllers;

use app\modules\gus\models\CreateParsterTaskForm;
use app\modules\gus\models\GusMaker;
use app\modules\gus\models\YandexAccount;
use app\modules\gus\urlRules\Sote;
use Yii;
use app\modules\gus\models\ParserTask;
use app\modules\gus\models\ParserTaskSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ParserController implements the CRUD actions for ParserTask model.
 */
class ParserController extends \app\components\controllers\AdminController
{
    public function behaviors()
    {
        return \yii\helpers\ArrayHelper::merge(
            parent::behaviors(),
            [
                'access' => [
                    'class' => \yii\filters\AccessControl::className(),
                    'rules' => [
                        [
                            'allow' => true,
                            'roles' => ['admin'],
                        ],
                    ],
                ]
            ]
        );
    }

    /**
     * Lists all ParserTask models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ParserTaskSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $this->view->title = 'Парсер';
        $this->view->params['breadcrumbs'][] = $this->view->title;

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new ParserTask model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new CreateParsterTaskForm();

        $translatorKeys = array_map(
            function($e) {
                return $e["translate_token"];
            },
            YandexAccount::find()->active()->asArray()->all()
        );

        $translator = new \nofuture17\Translate\Translator($translatorKeys);
        $supportedLanguages = $translator->getSupportedLanguages();
        $supportedLanguages = array_merge(
            ['0' => 'Без перевода'],
            $supportedLanguages['dirs']
        );


        if ($model->load(\Yii::$app->request->post())) {
            if ($model->translate_language != 0) {
                $translate_language = $supportedLanguages[$model->translate_language];
            } else {
                $translate_language = false;
            }

            $gusMaker = new GusMaker();
            $gusMaker->maxSoteSize = \Yii::$app->params["GUS.MAX_ITEMS"];
            $gusMaker->minSoteSize = \Yii::$app->params["GUS.MIN_ITEMS"];
            $gusMaker->soteModel = new \app\modules\gus\models\Sote();

            $parser = new \nofuture17\parsers\ContentParser([
                'startUrl' => $model->startUrl,
                'period' => 20,
                'periodCallable' => function($parser) use ($gusMaker, $translator, $translate_language) {
                    $result = $parser->getResult();
                    $items = [];
                    foreach ($result as $item) {
                        $newItem = [];
                        foreach ($item as $attribute => $value) {
                            $newValue = is_array($value) ? implode(PHP_EOL, $value) : $value;
                            $newValue = strip_tags($newValue);
                            if ($translate_language) {
                                $translation = $translator->translate($newValue, $translate_language);
                                $newItem[$attribute] = $translation->getResult()[0];
                            } else {
                                $newItem[$attribute] = strip_tags($newValue);
                            }
                        }
                        $items[] = $newItem;
                    }

                    $parser->clearResult();

                    $gusMaker->make($items);
                },
                'selectors' => $model->getSelectors(),
                'filterUrlRules' => $model->getFilterUrlRules('filterUrlRules'),
                'filterUrlToParseRules' => $model->getFilterUrlRules('filterUrlToParseRules'),
            ]);
            $parser->run();
            return $this->redirect(['index']);
        } else {

            $this->view->title = 'Добавить задачу парсера';
            $this->view->params['breadcrumbs'][] = ['label' => 'Парсер', 'url' => ['index']];
            $this->view->params['breadcrumbs'][] = $this->view->title;

            return $this->render('create', [
                'model' => $model,
                'supportedLanguages' => $supportedLanguages
            ]);
        }
    }

    /**
     * Updates an existing ParserTask model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if (\Yii::$app->request->isAjax) {
            $this->updateModelAjax($model);
        } else {
            if ($this->updateModel($model)) {
                return $this->redirect(['index']);
            } else {

                $this->view->title = 'Редактировать задачу парсера: ' . ' ' . $model->name;
                $this->view->params['breadcrumbs'][] = ['label' => 'Парсер', 'url' => ['index']];
                $this->view->params['breadcrumbs'][] = $this->view->title;

                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Deletes an existing ParserTask model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ParserTask model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ParserTask the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ParserTask::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
