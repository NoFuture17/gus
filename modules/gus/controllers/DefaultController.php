<?php

namespace app\modules\gus\controllers;

use app\modules\gus\models\Sote;
use yii\web\Controller;

class DefaultController extends Controller
{
    public function actionIndex($url = null)
    {
        if (empty($url)) {
            $item = Sote::find()->getBehavior('nestedCategoryQuery')->roots()->one();
        } else {
            $urlItems =  explode('/', $url);
            $itemId = array_pop($urlItems);
            $item = Sote::find()->active()->byPk($itemId)->one();

            if ($item->getBehavior('nestedSotes')->isRoot()) {
                \Yii::$app->response->redirect('/gus', 301);
            }
        }

        $nestedManager = $item->getBehavior('nestedSotes');
        $children = $nestedManager->children(1)->all();

        $left = null;
        $right = null;

        if (!$nestedManager->isRoot()) {
            $this->view->params['breadcrumbs'] = $nestedManager->getBreadcrumbsItems();

            $left = $nestedManager->prev()->one();
            if (empty($left)) {
                $left = $nestedManager->lastSibling()->one();
            }

            $right = $nestedManager->next()->one();
            if (empty($right)) {
                $right = $nestedManager->firstSibling()->one();
            }
        }

        $this->view->title = $item->meta_title;

        return $this->render(
            'index',
            [
                'page' => $item,
                'children' => $children,
                'right' => $right,
                'left' => $left,
            ]
        );
    }
}
