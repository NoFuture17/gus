<?php
/**
 * Created by PhpStorm.
 * User: nofuture17
 * Date: 26.02.16
 * Time: 10:00
 */

namespace app\modules\gus\urlRules;

use yii\web\UrlRuleInterface;
use yii\base\Object;
use app\modules\gus\models\Sote as Page;


class Sote extends Object implements UrlRuleInterface
{
    public function createUrl($manager, $route, $params)
    {
        if ($route === 'gus/default/index' && isset($params['url'])) {
            if (\Yii::$app->params['GUS.REVERS'] == '0') {
                return 'gus' . $params['url'];
            } else {

            }
        }

        return false;  // this rule does not apply
    }

    public function parseRequest($manager, $request)
    {
        $pathInfo = $request->getPathInfo();

        if (\Yii::$app->params['GUS.REVERS'] == '0') {
            if (preg_match('%^gus([/\d\w]*)$%', $pathInfo, $matches)) {

                $urlString = $matches[1];
                $urlItems = array_filter(
                    explode('/', $urlString),
                    function ($e) {
                        if (!empty($e)) {
                            return $e;
                        }
                    }
                );

                if (!empty($urlItems))
                    return ['gus/default/index', ['url' => $urlString]];
                else {
                    return ['gus/default/index', ['url' => null]];
                }
            }
        } else {

        }

        return false;  // this rule does not apply
    }
}