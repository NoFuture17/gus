<div class="gus-default-index">

    <div class="row sote">
        <h1 class="sote__title"><?= $page->name ?></h1>

        <div class="col-md-3 sote__children">
            <?php if (!empty($children)) : ?>

                <ul class="sote__children-list children-list">

                    <?php foreach ($children as $child) : ?>

                        <li class="children-list__item">
                            <a href="<?= $child->getItemUrl(); ?>" class="children-list__item-link">
                                <?= !empty($child->name_small) ? $child->name_small : $child->name; ?>
                            </a>
                        </li>

                    <?php endforeach; ?>

                </ul>

            <?php endif; ?>
        </div>

        <div class="col-md-6 sote__content">

            <?= $page->text_full; ?>

            <?php if (!empty($left) || !empty($right)) : ?>
                <div class="row sote__sibilng">

                    <div class="col-md-6 sote__sibilng-item sibilng-item">
                        <a href="<?= $left->getItemUrl(); ?>" class="sibilng-item__item-link">
                            <?= !empty($left->name_small) ? $left->name_small : $left->name; ?>
                        </a>
                    </div>

                    <div class="col-md-6 sote__sibilng-item sibilng-item">
                        <a href="<?= $right->getItemUrl(); ?>" class="sibilng-item__item-link">
                            <?= !empty($right->name_small) ? $right->name_small : $right->name; ?>
                        </a>
                    </div>

                </div>
            <?php endif; ?>

        </div>

        <div class="col-md-3 sote__other">

        </div>
    </div>
</div>
