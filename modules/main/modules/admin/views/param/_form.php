<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use \yii\helpers\Url;
use \app\components\widgets\form\SmartInput;

/* @var $this yii\web\View */
/* @var $model app\modules\main\models\Param */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="param-form">

    <?php
        $formConfig = [];

//        if (\Yii::$app->request->isAjax) {
//            $formConfig['action'] = Url::to(['/main/admin/param/update', 'id' => $model->id]);
//            $formConfig['options']['data-ajax-action'] = 'online-change';
//        }

        $form = ActiveForm::begin($formConfig);
    ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?php
        echo SmartInput::widget([
            'model'     => $model,
            'attribute' => 'value',
            'label'     => true,
            'form'      => $form,
        ]);
    ?>

    <?php if ($model->isNewRecord): ?>
        <?= $form->field($model, 'default')->textarea(['rows' => 6]) ?>

        <?= $form->field($model, 'param')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'type')->textInput(['maxlength' => true]) ?>
    <?php endif; ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
