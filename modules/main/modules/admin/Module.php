<?php

namespace app\modules\main\modules\admin;

class Module extends \app\components\modules\Admin
{
    public $controllerNamespace = 'app\modules\main\modules\admin\controllers';

    public static function getMenuItems() {
        return [
            'section' => 'admin',
            'items' => [
                [
                    'label' => 'Общее',
                    'items' => [
                        ['label' => 'Настройки', 'url' => ['/main/admin/param/index']],
                        ['label' => 'Языки', 'url' => ['/main/admin/language/index']],
                        ['label' => 'Редиректы', 'url' => ['/main/admin/redirect/index']],
                        ['label' => 'Сбросить кэш', 'url' => ['/main/admin/cache/flush']],
                        ['label' => 'Менеджер файлов', 'url' => ['/main/elfinder/manager']],
                        [
                            'label' => 'Меню',
                            'items' => [
                                ['label' => 'Секции', 'url' => ['/main/admin/menu/index']],
                                ['label' => 'Элементы', 'url' => ['/main/admin/menu-item/index']],
                            ],
                        ]
                    ]
                ]
            ],
        ];
    }

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
