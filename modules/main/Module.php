<?php

namespace app\modules\main;

use \app\modules\main\components\SiteConfig;
use \app\modules\main\components\Redirector;
use Yii;

/**
 * Description of Main
 *
 * @author nofuture17
 */
class Module extends \app\components\modules\Module implements \yii\base\BootstrapInterface
{
    public $theme = 'base';

    public function bootstrap($app)
    {
        parent::bootstrap($app);

        // если это не веб приложение, нам не нужно устанавливать тему
        if (($app instanceof \yii\web\Application)) {
            // устанавливаем выбранную тему
            $this->_setTheme($app);
            // Редиректы
            $this->_redirects($app);
            // Менюшки из базы
            $this->_setMenu();
        }

        // устанавливаем конфиг
        $this->_setConfig($app);
    }

    /**
     * Устанавливает тему приложения из переменной self::$theme
     * @param \yii\web\Application $app
     * @return boolean
     */
    protected function _setTheme($app)
    {

        $app->view->theme = Yii::createObject([
            'class' => '\yii\base\Theme',
            'pathMap' => [
                '@app/views' => "@app/themes/{$this->theme}",
                '@app/modules' => "@app/themes/{$this->theme}/modules",
                '@app/widgets' => "@app/themes/{$this->theme}/widgets"
            ],
            'baseUrl' => "@web/themes/{$this->theme}",
        ]);

        return true;
    }

    /**
     * Выполняем редирект (если он нужен)
     * @param $app \yii\web\Application
     */
    protected function _redirects($app)
    {
        $redirector = new Redirector($app->request->url);
        $redirector->run();
    }

    /**
     * Выбирает из базы все секции меню и добавляет в компонент меню
     */
    protected function _setMenu()
    {
        $sections = \app\modules\main\models\Menu::find()->active()->all();

        function recursive($items)
        {
            $result = [];
            foreach ($items as $item) {
                $menuItem = [
                    'label' => $item->name,
                    'url' => $item->url,
                    'options' => (!empty($item->options)) ? $item->options : [],
                    'linkOptions' => (!empty($item->linkOptions)) ? $item->linkOptions : [],
                    'dropDownOptions' => (!empty($item->dropDownOptions)) ? $item->dropDownOptions : [],
                ];

                if (!empty($item->url)) {
                    $menuItem[] = $item->url;
                }

                if (!empty($item->getChildren()->all())) {
                    $menuItem['items'] = recursive($item->getChildren()->all());
                }

                $result[] = $menuItem;
            }
            return $result;
        }

        ;

        foreach ($sections as $section) {
            $items = $section->getMenuItems()->all();

            if (!empty($items)) {
                $result = [
                    'section' => $section->section,
                    'items' => recursive($items)
                ];
            }
        }

        if (!empty($result)) {
            \app\components\widgets\Menu::getInstance()->addItems($result);
        }
    }

    /**
     * Устанавливает конфиг, полученный из базы (ну или из корована)
     * @param \yii\web\Application $app
     */
    protected function _setConfig($app)
    {
        $config = SiteConfig::getConfig();

        if (isset($config['MAILER.FILE_TRANSPORT'])) {
            $mailer = \Yii::$app->mailer;
            $mailer->useFileTransport = (boolean)$config['MAILER.FILE_TRANSPORT'];

            if (!$mailer->useFileTransport) {
                $mailer->transport->setAuthMode('login');
                $mailer->transport->setHost($config['MAILER.HOST']);
                $mailer->transport->setPort($config['MAILER.PORT']);
                $mailer->transport->setEncryption($config['MAILER.ENCRYPTION']);
                $mailer->transport->setUsername($config['MAILER.USERNAME']);
                $mailer->transport->setPassword($config['MAILER.PASSWORD']);
            }
        }

        if (!empty($config['THEME'])) {
            $this->theme = $config['THEME'];
        }

        if (!empty($config['APP.NAME'])) {
            $app->name = $config['APP.NAME'];
        }

        foreach ($config as $key => $param) {
            if (strstr($key, 'PARAM'))
                $app->params[str_replace('PARAM.', '', $key)] = $param;
        }
    }

    public function init()
    {
        parent::init();
        $this->controllerMap = \yii\helpers\ArrayHelper::merge(
            [
                'elfinder' => [
                    'class' => 'mihaildev\elfinder\Controller',
                    'layout' => '@app/views/layouts/admin',
                    'access' => ['admin'], //глобальный доступ к фаил менеджеру @ - для авторизорованных , ? - для гостей , чтоб открыть всем ['@', '?']
                    'disabledCommands' => ['netmount'], //отключение ненужных команд https://github.com/Studio-42/elFinder/wiki/Client-configuration-options#commands
                    'roots' => [
                        [
                            'baseUrl' => '@web',
                            'basePath' => '@webroot',
                            'path' => 'data',
                            'name' => 'Global'
                        ],

                    ],
                    'managerOptions' => [
                        'validName' => '/^[^.]{1}.*$/',
                    ],
                ]
            ],
            $this->controllerMap
        );

        $this->modules = [
            'admin' => [
                'class' => '\app\modules\main\modules\admin\Module',
            ],
        ];
    }

    public function getUrlRules()
    {
        return array_merge(
            parent::getUrlRules(),
            [
                [
                    'rules' => [
                        '/admin' => '/main/admin/default/index'
                    ],
                    'append' => true,
                ],
            ]
        );
    }
}