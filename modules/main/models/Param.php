<?php

namespace app\modules\main\models;

use Yii;
use \app\components\widgets\form\SmartInput;
use \yii\helpers\FileHelper;

/**
 * This is the model class for table "param".
 *
 * @property integer $id
 * @property string $param
 * @property string $value
 * @property string $default
 * @property string $name
 * @property string $type
 * @property string $input_type
 */
class Param extends \app\components\models\SmartRecord
{

    /**
     * Возвращает список существующих тем
     * @return int|null
     */
    public static function getThemes() {
        $result = [];

        if ($handle = opendir(\Yii::getAlias('@app/themes'))) {
            /* Именно этот способ чтения элементов каталога является правильным. */
            while (false !== ($file = readdir($handle))) {
                if (!in_array($file, ['.', '..']) && !is_file($file))
                    $result[] = $file;
            }

            closedir($handle);
        }

        return $result;
    }

    // Методы SmartInputInterface
    public function getInputType($attribute)
    {
        $result = null;

        switch ($attribute) {
            case 'value' :
                if (in_array($this->type, ['theme', 'boolean', 'select'])) {
                    $result = SmartInput::TYPE_SELECT;
                } elseif (in_array($this->type, ['boolean'])) {
                    $result = SmartInput::TYPE_SELECT;
                }
                break;
            default:
                $result = parent::getInputType($attribute);
        }

        return $result;
    }

    public function getInputData($attribute)
    {
        $result = null;

        switch ($attribute) {
            case 'value' :
                if ($this->type == 'theme') {
                    $result = self::getThemes();
                } elseif ($this->type == 'boolean') {
                    $result = self::getActiveArray();
                } elseif ($this->param == 'PARAM.SITE.LANGUAGE') {
                    $result = Language::getDb()->cache(function ($db) {
                        return array_merge(
                            ['0' => 'Не выбран'],
                            \yii\helpers\ArrayHelper::map(Language::find()->all(), 'id', 'name')
                        );
                    });
                }
                break;
            default:
                $result = parent::getInputData($attribute);
        }

        return $result;
    }

    public function getInputOptions($attribute)
    {
        return parent::getInputOptions($attribute);
    }



    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'param';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(
            parent::rules(),
            [
                [['param', 'value', 'default', 'name', 'type'], 'required'],
                [['value', 'default'], 'string'],
                [['param', 'type'], 'string', 'max' => 128],
                [['name'], 'string', 'max' => 255],
                [['param'], 'unique']
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
            parent::attributeLabels(),
            [
                'param' => 'Машинное имя',
                'value' => 'Значение',
                'default' => 'По умолчанию',
                'name' => 'Имя',
                'type' => 'Тип',
            ]
        );
    }

    /**
     * @inheritdoc
     * @return ParamQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ParamQuery(get_called_class());
    }
}
