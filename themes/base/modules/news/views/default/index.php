<div class="news-default-index">
    <h1>Новости</h1>
    <?php
        if (!empty($news))
        foreach ($news as $item):
    ?>
            <div class="news">
                <div class="news__title"><?= $item->name ?></div>
                <div class="news__content"><?= $item->text_full ?></div>
            </div>
    <?php
        endforeach;
    ?>
</div>
