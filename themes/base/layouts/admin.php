<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use \app\components\widgets\Menu;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <link rel="stylesheet" href="/resources/css/common.css">
    <link rel="stylesheet" href="/resources/css/admin.css">
    <link rel="stylesheet" href="/resources/plugins/dropzone/dropzone.css">
    <link rel="stylesheet" href="/resources/plugins/magnific-popup/magnific-popup.css">
    <link rel="stylesheet" type="text/css" href="/resources/plugins/datatables/DataTables-1.10.10/css/dataTables.bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="/resources/plugins/chosen/chosen.min.css"/>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => Yii::$app->name,
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-top',
        ],
    ]);
    echo Menu::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'section' => 'admin',
        'items' => [
            Yii::$app->user->isGuest ?
                ['label' => 'Login', 'url' => ['/login']] :
                [
                    'label' => 'Logout (' . Yii::$app->user->identity->email . ')',
                    'url' => ['/logout'],
                    'linkOptions' => ['data-method' => 'post']
                ],
        ],
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>

        <?= \app\components\widgets\Alert::widget() ?>

        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; My Company <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>

<script src="/resources/js/lib.js"></script>
<script src="/resources/js/admin.js"></script>
<script src="/resources/plugins/magnific-popup/jquery.magnific-popup.min.js"></script>
<script src="/resources/plugins/dropzone/dropzone.js"></script>
<script type="text/javascript" src="/resources/plugins/datatables/datatables.min.js"></script>
<script type="text/javascript" src="/resources/plugins/datatables/DataTables-1.10.10/js/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" src="/resources/plugins/chosen/chosen.jquery.js"></script>

</body>
</html>
<?php $this->endPage() ?>
