<?php

$params = [];

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'language'=>'ru-RU',
    'bootstrap' => [
        'log',
        function () {
            return Yii::$app->getModule('main');
        },
        function () {
            return Yii::$app->getModule('user');
        },
        function () {
            return Yii::$app->getModule('page');
        },
        function () {
            return Yii::$app->getModule('gus');
        },
    ],
    'modules' => [
        'main' => [
            'class' => 'app\modules\main\Module',
        ],
        'user' => [
            'class' => 'app\modules\user\Module',
        ],
        'page' => [
            'class' => 'app\modules\page\Module',
        ],
        'gus' => [
            'class' => 'app\modules\gus\Module',
        ],

    ],
    'components' => [
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
            'cache' => 'cache' //Включаем кеширование
        ],
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'HNXsVFFXZCp_NBM3c-_-82mltu8SFQtL',
        ],
        'cache' => [
            'class' => YII_DEBUG ? 'yii\caching\DummyCache' : 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\modules\user\models\User',
            'enableAutoLogin' => true,
            'loginUrl' => ['/user/default/login'],
        ],
        'errorHandler' => [
            'errorAction' => '/main/default/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
            'transport' => [
                'class' => 'Swift_SmtpTransport',
            ],
        ],
        'urlManager' => [
            'class' => 'yii\web\UrlManager',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                '<module:[\w\-]+>/<controller:[\w\-]+>/<id:\d+>' => '<module>/<controller>/view',
                '<module:[\w\-]+>/<controller:[\w\-]+>/<action:[\w\-]+>/<id:\d+>' => '<module>/<controller>/<action>',
                '<module:[\w\-]+>/<controller:[\w\-]+>' => '<module>/<controller>/index',
            ],
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'formatter' => [
            'datetimeFormat' => 'php:d.m.Y H:i:s',
            'dateFormat' => 'php:d.m.Y',
            'timeFormat' => 'php:H:i:s',
        ],
        'adminMenu' => [
            'class' => 'app\components\widgets\AdminMenu'
        ],
        'db' => require(__DIR__ . '/db.php'),
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;
