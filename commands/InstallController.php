<?php
/**
 * Created by PhpStorm.
 * User: nofuture17
 * Date: 03.12.15
 * Time: 15:46
 */

namespace app\commands;

use yii\console\Controller;
use Yii;

class InstallController extends Controller
{
    public $defaultAction = 'install';

    public function actionInstall()
    {
        $this->stdout("Update composer:\n");
        $this->_updateComposer();
        $this->stdout("Migrations up:!\n");
        $this->_migrateRbac();
        $this->_upMigrations();
        $this->stdout("Init RBAC!\n");
        $this->_initRbac();
        $this->stdout("OK!\n");
    }

    public function actionUpdate()
    {

    }

    protected function _updateComposer() {
        $appDir = Yii::getAlias('@app');

        if (!file_exists($appDir . '/composer.phar')) {
            `cd {$appDir} && php -r "readfile('https://getcomposer.org/installer');" | php`;
        }

        `{$appDir}/composer.phar global require "fxp/composer-asset-plugin:~1.1.1"`;

        if (file_exists($appDir . '/composer.lock')) {
            `{$appDir}/composer.phar update`;
        } else {
            `{$appDir}/composer.phar install`;
        }
    }

    protected function _upMigrations() {
        $this->run('migrate/up', ['interactive' => false]);
    }

    protected function _migrateRbac()
    {
        $this->run('rbac/migrate');
    }

    protected function _initRbac() {
        $this->run('rbac/init');
    }
}